MLRL is a challenging roguelike game where you become a hero trying to complete the 50 floors of the dungeon, starting at sewers and going deeper and deeper until you'll reach the caves and their demonic enemies.

Features:
   + Procedurally generated levels (every run is unique)
   + Permadeath
   + 3 unique classes of hero
   + 50 floors of increasing difficulty
   + More than 25 different enemies with their own quirks
   + 5 powerful bosses to defeat
   + An endless mode to push your limits
   + Possibility to switch between graphic and ascii mode while playing
   + Pixel art graphics
