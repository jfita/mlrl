/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.adapter.AbstractListAdapter;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.KindOfWeapon;

import java.util.Comparator;

class InventoryAdapter extends AbstractListAdapter<Item, VisTable> implements InventoryEditor.Listener {
    private InventoryEditor editor;
    private final Drawable bg = VisUI.getSkin().getDrawable("window-bg");
    private final Drawable selection = VisUI.getSkin().getDrawable("list-selection");

    InventoryAdapter() {
        this(new InventoryEditor());
    }

    InventoryAdapter(InventoryEditor editor) {
        this.editor = editor;
        this.editor.addListener(this);
    }

    public void setEditor(InventoryEditor editor) {
        this.editor.removeListener(this);
        this.editor = editor;
        this.editor.addListener(this);
        itemsChanged();
    }

    public void setInventory(Inventory inventory) {
        editor.setInventory(inventory);
    }

    public void clear() {
        setInventory(new Inventory());
    }

    @Override
    protected void sort(Comparator<Item> comparator) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected VisTable createView(Item item) {
        VisTable table = new VisTable();
        table.left();
        table.add(new VisLabel(item.getName()));
        if (item instanceof KindOfWeapon) {
            KindOfWeapon kow = (KindOfWeapon) item;
            table.add(new VisLabel(" [STR:" + kow.getBonusStr()));
            table.add(new VisLabel(" DEX:" + kow.getBonusDex()));
            table.add(new VisLabel(" WIS:" + kow.getBonusWis()));
            table.add(new VisLabel(" CON:" + kow.getBonusCon() + "]"));
        }
        return table;
    }

    @Override
    public Iterable<Item> iterable() {
        return editor.iterable();
    }

    @Override
    public int size() {
        return editor.size();
    }

    @Override
    public int indexOf(Item item) {
        return editor.indexOf(item);
    }

    public void reroll() {
        editor.reroll();
    }

    @Override
    public void add(Item item) {
        editor.add(item);
    }

    @Override
    public Item get(int index) {
        return editor.get(index);
    }

    @Override
    protected void selectView(VisTable view) {
        view.setBackground(selection);
    }

    @Override
    protected void deselectView(VisTable view) {
        view.setBackground(bg);
    }

    @Override
    public void onItemsChanged() {
        itemsChanged();
    }

    @Override
    public void onItemAdded(Item item) {
        itemAdded(item);
    }

    @Override
    public void onItemsRemoved(Iterable<Item> items) {
        for (Item item : items) {
            itemRemoved(item);
        }
    }

    public void dropSelected() {
        editor.drop(getSelection());
    }
}
