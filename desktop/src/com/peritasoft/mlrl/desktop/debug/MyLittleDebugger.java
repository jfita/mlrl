/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.TableUtils;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPane;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPaneAdapter;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.render.GameRenderer;
import com.peritasoft.mlrl.screens.MlrlScreen;
import com.peritasoft.mlrl.screens.PlayScreen;

public class MyLittleDebugger extends MyLittleRogueLike
        implements GeneratorTab.Listener
        , RendererTab.Listener
        , LevelTab.Listener
        , GestureDetector.GestureListener {
    private Stage stage;
    private Preferences preferences;
    private GameRenderer renderer;
    private VisWindow debugWindow;
    private InventoryEditWindow inventoryEditWindow;
    private GestureDetector gestureDetector;
    private LevelTab levelTab;

    public MyLittleDebugger(MyLittleRogueLike.Config config) {
        super(config);
    }

    @Override
    public void create() {
        super.create();
        preferences = Gdx.app.getPreferences("mlrl-debugger");
        renderer = new GameRenderer(getBatch());
        gestureDetector = new GestureDetector(this);
        setupStage();
    }

    @Override
    protected MlrlScreen startScreen() {
        // No start screen for debugger
        return null;
    }

    private void setupStage() {
        VisUI.load(VisUI.SkinScale.X1);

        stage = new Stage(new ScreenViewport(), getBatch());

        inventoryEditWindow = new InventoryEditWindow();

        debugWindow = new VisWindow("My Little Debugger", "resizable");
        debugWindow.setResizable(true);
        debugWindow.setCenterOnAdd(true);
        TableUtils.setSpacingDefaults(debugWindow);

        final VisTable container = new VisTable();

        final TabbedPane tabbedPane = new TabbedPane();
        tabbedPane.addListener(new TabbedPaneAdapter() {
            @Override
            public void switchedTab(Tab tab) {
                container.clearChildren();
                container.add(tab.getContentTable()).expandX().fillX();
                container.row();
                container.add().expand().fill();
            }
        });

        debugWindow.add(tabbedPane.getTable()).expandX().fillX();
        debugWindow.row();
        debugWindow.add(container).expand().fill();
        debugWindow.row();

        levelTab = new LevelTab(this);

        tabbedPane.add(new GeneratorTab(json, new PrefixedPreferences(preferences, "generator"), this));
        tabbedPane.add(new RendererTab(new PrefixedPreferences(preferences, "renderer"), this));
        tabbedPane.add(levelTab);
        tabbedPane.switchTab(0);

        debugWindow.setSize(400, 500);
        stage.addActor(debugWindow);
    }

    @Override
    public void generateLevel(long seed, int floor, int width, int height, final PlayerHero hero, int heroLevel) {
        if (screen != null) {
            screen.dispose();
        }
        PlayScreen playScreen = new PlayScreen(this, hero, width, height, floor, seed, renderer, false);
        // We must level up *after* creating the screen because there
        // are GameEventListeners that relay on GameEvent.levelUp to work.
        hero.addXP((heroLevel - 1) * 100);
        setScreen(playScreen);
    }

    @Override
    public void openInventoryEditWindow(InventoryEditor editor) {
        if (!inventoryEditWindow.hasParent()) {
            stage.addActor(inventoryEditWindow);
        } else {
            stage.setKeyboardFocus(inventoryEditWindow);
            inventoryEditWindow.toFront();
        }
        inventoryEditWindow.setEditor(editor);
    }

    @Override
    public void enableFieldOfView(boolean enable) {
        resetPan();
        renderer.enableFieldOfView(enable);
    }

    @Override
    public void enableCenterOnPlayer(boolean enable) {
        renderer.enableCenterOnPlayerOnce(enable);
    }


    @Override
    public void resetPan() {
        renderer.resetPan();
    }

    @Override
    public void killCharacter() {
        final Cell cell = getTouchCell();
        if (cell == null || !cell.hasCharacter()) return;
        final Character character = cell.getCharacter();
        character.receiveHit(character.getHp(), null);
        screen.keyDown(Input.Keys.L);
        levelTab.setCell(cell);
    }

    @Override
    public void render() {
        super.render();
        stage.act(Gdx.graphics.getDeltaTime());
        if (screen == null) {
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }
        stage.getViewport().apply(true);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        renderer.dispose();
        stage.dispose();
        VisUI.dispose();
        super.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.F12) {
            if (debugWindow.hasParent()) {
                debugWindow.remove();
            } else {
                stage.addActor(debugWindow);
            }
            return true;
        }
        return stage.keyDown(keycode) || super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        return stage.keyUp(keycode) || super.keyUp(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        return stage.keyTyped(character) || super.keyTyped(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return stage.touchDown(screenX, screenY, pointer, button) || gestureDetector.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return stage.touchUp(screenX, screenY, pointer, button) || gestureDetector.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return stage.touchDragged(screenX, screenY, pointer) || gestureDetector.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return stage.mouseMoved(screenX, screenY) || gestureDetector.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return stage.scrolled(amountX, amountY) || gestureDetector.scrolled(amountX, amountY);
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        if (!debugWindow.hasParent() || !(screen instanceof PlayScreen)) {
            return super.touchDown((int) x, (int) y, Input.Buttons.LEFT, button);
        }
        touchPoint.set(x, y);
        viewport.unproject(touchPoint);
        final Cell cell = getTouchCell();
        levelTab.setCell(cell);
        return true;
    }

    protected Cell getTouchCell() {
        final PlayScreen playScreen = (PlayScreen) screen;
        final Position position = playScreen.getLevelPosition(touchPoint.x, touchPoint.y);
        final Level level = playScreen.getCurrentLevel();
        return level.getCell(position.getX(), position.getY());
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        float scaleX = viewport.getWorldWidth() / (float) viewport.getScreenWidth();
        float scaleY = viewport.getWorldHeight() / (float) viewport.getScreenHeight();
        renderer.pan(deltaX * scaleX, -deltaY * scaleY);
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
