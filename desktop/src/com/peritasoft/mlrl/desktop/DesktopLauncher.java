/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.desktop.debug.MyLittleDebugger;

import java.util.Arrays;

public class DesktopLauncher {
    public static void main(String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        boolean debug = arg.length > 0 && Arrays.asList(arg).contains("--debug");
        config.setTitle("My Little RogueLike");
        config.setWindowedMode(debug ? 1024 : 800, debug ? 1024 : 480);
        config.setWindowIcon(
                "icon128.png",
                "icon32.png",
                "icon16.png"
        );
        final MyLittleRogueLike.Config mlrlConfig = new MyLittleRogueLike.Config();
        new Lwjgl3Application(debug ? new MyLittleDebugger(mlrlConfig) : new MyLittleRogueLike(mlrlConfig), config);
    }
}
