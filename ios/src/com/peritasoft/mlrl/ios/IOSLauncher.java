package com.peritasoft.mlrl.ios;

import com.peritasoft.mlrl.MyLittleRogueLike;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.uikit.UIApplication;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class IOSLauncher extends IOSApplication.Delegate {
    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        final MyLittleRogueLike.Config mlrlConfig = new MyLittleRogueLike.Config();
        mlrlConfig.showPad = true;
        return new IOSApplication(new MyLittleRogueLike(mlrlConfig), config);
    }

    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
}