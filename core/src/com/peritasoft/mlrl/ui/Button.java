/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;

public class Button implements Widget {
    private final Rectangle rect;
    private final NinePatch unpressedNinePatch;
    private final NinePatch pressedNinePatch;
    private final String label;
    private final Vector2 offset;

    public Button(float x, float y, float width, NinePatch ninePatch, String label) {
        this(x, y, width, ninePatch, ninePatch, label);
    }

    public Button(float x, float y, float width, NinePatch unpressedNinePatch, NinePatch pressedNinePatch,
                  String label) {
        this.rect = new Rectangle(x, y, width, 16f);
        this.unpressedNinePatch = unpressedNinePatch;
        this.pressedNinePatch = pressedNinePatch;
        this.label = label;
        this.offset = new Vector2(0f, 0f);
    }

    @Override
    public Rectangle getRect() {
        return rect;
    }

    public void draw(Batch batch, BitmapFont font) {
        draw(batch, font, false);
    }

    public void draw(Batch batch, BitmapFont font, boolean pressed) {
        draw(batch, font, label, pressed);
    }

    public void draw(Batch batch, BitmapFont font, String string) {
        draw(batch, font, string, false);
    }

    public void draw(Batch batch, BitmapFont font, String string, boolean isPressed) {
        final NinePatch ninePatch = isPressed ? pressedNinePatch : unpressedNinePatch;
        ninePatch.draw(batch, rect.x + offset.x, rect.y + offset.y, rect.width, rect.height);
        font.draw(batch, string, rect.x + offset.x, rect.y + font.getLineHeight() + 5 + offset.y, rect.width, Align.center, false);
    }

    public boolean pressed(Vector2 position) {
        return rect.contains(position);
    }

    public void setOffset(float offsetX, float offsetY) {
        offset.set(offsetX, offsetY);
    }

    public Vector2 getOffset() {
        return offset;
    }
}
