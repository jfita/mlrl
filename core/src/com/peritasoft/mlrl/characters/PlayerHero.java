/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.PathFinder;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Crosshairs;
import com.peritasoft.mlrl.effects.PlayerRests;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.PotionColour;
import com.peritasoft.mlrl.props.Prop;
import com.peritasoft.mlrl.weapons.Shootable;

import java.util.EnumSet;
import java.util.Queue;

public class PlayerHero extends Character {

    private final Sex sex;
    private final String name;
    private float timer;
    int xp;
    private Crosshairs crosshairs;
    private EnumSet<PotionColour> knownPotions;
    private Queue<Position> path;
    private Character killer;

    public PlayerHero(Demography demography, Sex sex, int level, int str, int dex, int wis, int con) {
        this(demography, sex, level, str, dex, wis, con, new Inventory());
    }

    public PlayerHero(Demography demography, Sex sex, int level, int str, int dex, int wis, int con, Inventory inventory) {
        super(demography, level, str, dex, wis, con, demography == Demography.HERO_WARRIOR ? 4 : 5, 0, 0, inventory);
        this.sex = sex;
        this.name = demography.getNameBySex(sex);
        this.xp = 0;
        this.knownPotions = EnumSet.noneOf(PotionColour.class);
    }

    public static int initialStr(Demography demography) {
        switch (demography) {
            case HERO_WARRIOR:
                return 3;
            case HERO_ARCHER:
                return 2;
            case HERO_MAGE:
                return 1;
            default:
                return 0;
        }
    }

    public static int initialDex(Demography demography) {
        switch (demography) {
            case HERO_WARRIOR:
            case HERO_MAGE:
                return 2;
            case HERO_ARCHER:
                return 4;
            default:
                return 0;
        }
    }

    public static int initialWis(Demography demography) {
        switch (demography) {
            case HERO_WARRIOR:
            case HERO_ARCHER:
                return 1;
            case HERO_MAGE:
                return 5;
            default:
                return 0;
        }
    }

    public static int initialCon(Demography demography) {
        switch (demography) {
            case HERO_WARRIOR:
                return 4;
            case HERO_ARCHER:
                return 3;
            case HERO_MAGE:
                return 2;
            default:
                return 0;
        }
    }

    public int getXp() {
        return xp;
    }

    @Override
    public String getName() {
        return name;
    }

    public Sex getSex() {
        return sex;
    }

    public void addKnownPotion(PotionColour col) {
        knownPotions.add(col);
    }

    public boolean knowsPotion(PotionColour col) {
        return knownPotions.contains(col);
    }

    public Direction followPath(float delta) {
        super.update(delta);
        timer -= delta;
        if (timer <= 0 && hasPath() && !isAiming()) {
            timer = 0.15f;
            Position pos = path.remove();
            return getPosition().directionTo(pos);
        }
        return null;
    }

    public Action update(Level level, Key key) {
        this.getPrevPosition().set(getPosition());
        if (isAiming()) {
            switch (key) {
                case CANCEL:
                    stopAiming();
                    return Action.NONE;
                case FIRE:
                    if (shoot(level)) {
                        return Action.NONE;
                    } else {
                        return Action.ATTACK;
                    }
                case UP:
                    crosshairs.move(Direction.NORTH, level);
                    return Action.NONE;
                case LEFT:
                    crosshairs.move(Direction.WEST, level);
                    return Action.NONE;
                case DOWN:
                    crosshairs.move(Direction.SOUTH, level);
                    return Action.NONE;
                case RIGHT:
                    crosshairs.move(Direction.EAST, level);
                    return Action.NONE;
            }
        } else {
            switch (key) {
                case UP:
                    manaRegen();
                    return move(Direction.NORTH, level);
                case LEFT:
                    manaRegen();
                    return move(Direction.WEST, level);
                case DOWN:
                    manaRegen();
                    return move(Direction.SOUTH, level);
                case RIGHT:
                    manaRegen();
                    return move(Direction.EAST, level);
                case LOOK:
                    if (isPetrified()) {
                        return skipTurn();
                    }
                    GameEvent.lookedAt(level.getItem(getPositionX(), getPositionY()));
                    return Action.LOOK;
                case FIRE:
                    if (canFireWeapon()) {
                        aim(shootWeapon(), level);
                        return Action.NONE;
                    }
                    break;
            }
        }
        return Action.NONE;
    }

    private boolean shoot(Level level) {
        if (crosshairs.isAtInitialPosition()) {
            GameEvent.canNotShootSelf(this);
            return true;
        }
        manaRegen();
        boolean shoot = shoot(level, crosshairs);
        stopAiming();
        return shoot;
    }

    public Crosshairs getCrosshairs() {
        return crosshairs;
    }

    @Override
    public void aim(Shootable shootable, Level level) {
        Direction dir = getLastDirection();
        if (!level.isWalkable(getPositionX() + dir.x, getPositionY() + dir.y)) {
            dir = Direction.NONE;
        }
        crosshairs = new Crosshairs(getPosition(), dir, shootable);
        final Character closestVictim = findClosestVictim(level);
        if (closestVictim != null) {
            crosshairs.moveTo(closestVictim.getPosition(), level);
        }
    }

    public void stopAiming() {
        crosshairs = null;
    }

    public boolean isAiming() {
        return crosshairs != null;
    }

    @Override
    public void receiveHit(int damage, Character enemy) {
        super.receiveHit(damage, enemy);
        if (path != null) path.clear();
    }

    public void addXP(int xp) {
        this.xp += xp;
        while (this.xp >= 100) {
            this.xp -= 100;
            levelUp();
        }
    }

    public void levelUp() {
        int strUp = 0;
        int dexUp = 0;
        int wisUp = 0;
        int conUp = 0;

        if (getKlass() == Klass.WARRIOR) {
            if (MathUtils.randomBoolean(0.80f)) {
                strUp = MathUtils.random(1, 2);
                setStr(getStr() + strUp);
            }
            if (MathUtils.randomBoolean(0.65f)) {
                dexUp = MathUtils.random(1, 2);
                setDex(getDex() + dexUp);
            }
            if (MathUtils.randomBoolean(0.15f)) {
                wisUp = 1;
                setWis(getWis() + wisUp);
            }
            conUp = 2;
            setCon(getCon() + conUp);
        } else if (getKlass() == Klass.ARCHER) {
            if (MathUtils.randomBoolean(0.65f)) {
                strUp = 1;
                setStr(getStr() + strUp);
            }
            if (MathUtils.randomBoolean(0.80f)) {
                dexUp = MathUtils.random(1, 2);
                setDex(getDex() + dexUp);
            }
            if (MathUtils.randomBoolean(0.15f)) {
                wisUp = 1;
                setWis(getWis() + wisUp);
            }
            conUp = MathUtils.random(1, 2);
            setCon(getCon() + conUp);
        } else if (getKlass() == Klass.MAGE) {
            if (MathUtils.randomBoolean(0.15f)) {
                strUp = 1;
                setStr(getStr() + strUp);
            }
            if (MathUtils.randomBoolean(0.65f)) {
                dexUp = MathUtils.random(1, 2);
                setDex(getDex() + dexUp);
            }
            if (MathUtils.randomBoolean(0.80f)) {
                wisUp = MathUtils.random(1, 2);
                setWis(getWis() + wisUp);
            }
            conUp = 1;
            setCon(getCon() + conUp);
        }

        resetHp();
        fullMp();
        setLevel(getLevel() + 1);
        GameEvent.levelUp(getLevel(), strUp, dexUp, wisUp, conUp);
    }

    @Override
    protected boolean openProp(Cell cell, Level level) {
        Prop prop = cell.getProp();
        if (prop == null) {
            return false;
        }
        prop.open(this, level);
        return true;
    }

    @Override
    public void inventoryFull() {
        GameEvent.inventoryFull();
    }


    public void moveTo(Level level, Position position) {
        path = PathFinder.find(level, getPosition(), position);
    }

    public boolean hasPath() {
        return path != null && !path.isEmpty();
    }

    public void aimAt(Level level, Position position) {
        if (crosshairs == null) return;
        if (crosshairs.getPosition().equals(position)) {
            shoot(level);
            return;
        }
        crosshairs.moveTo(position, level);
    }

    public Character getKiller() {
        return killer;
    }

    public void killed(Character c) {
        addXP(c.getXp(getLevel()));
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        this.killer = killer;
        GameEvent.died(this);
        level.add(new PlayerRests(getPosition(), 60));
        return true;
    }

    @Override
    public boolean shouldPickup(Item item) {
        return true;
    }

    public void changeFloor(Level prevLevel, Level nextLevel, Position startingPosition) {
        if (prevLevel != null) {
            prevLevel.removeEnemy(this);
        }
        setPosition(startingPosition, nextLevel);
        resetPrevPosition();
    }

    public enum Key {
        UP,
        LEFT,
        DOWN,
        RIGHT,
        FIRE,
        LOOK,
        CANCEL,
    }
}
