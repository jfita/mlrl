/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.*;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.LevelType;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.KindOfWeapon;
import com.peritasoft.mlrl.weapons.FireGrimoire;
import com.peritasoft.mlrl.weapons.IceGrimoire;
import com.peritasoft.mlrl.weapons.WeaponGenerator;

public class Death extends Character {
    private int currentPersona;
    private final Persona[] personas;

    public Death(int level, Position pos, PlayerHero playerHero) {
        this(level, pos, playerHero, new Inventory());
    }

    Death(int level, Position pos, PlayerHero playerHero, Inventory inventory) {
        super(Demography.DEATH, level, 0, 0, 0, 0, 18, pos.getX(), pos.getY(),
                new WanderSeekApproach(true),
                inventory);
        this.setStr((int) (level * 2.5f));
        this.setDex((int) (level * 1.5f));
        this.setWis(level);
        this.setCon(level * 3);
        this.resetHp();
        final KindOfWeapon weapon = findBestWeapon(playerHero);
        final FireGrimoire fireGrimoire = WeaponGenerator.generateFireGrimoire(level);
        final IceGrimoire iceGrimoire = WeaponGenerator.generateIceGrimoire(level);
        inventory.add(fireGrimoire);
        inventory.add(iceGrimoire);
        inventory.add(weapon);
        currentPersona = 0;
        personas = new Persona[]{
                new Persona(Demography.DEATH, LevelType.DEATH, getMaxHp() - 1, new WanderSeekApproach(false), null),
                new Persona(Demography.SPIDER_GIANT, LevelType.SEWER, 5 * getMaxHp() / 6, new WanderSeekApproach(false), null),
                new Persona(Demography.GOBLIN_MAGE, LevelType.DUNGEON, 4 * getMaxHp() / 6, new StayAtRangeAndShoot(MathUtils.randomBoolean() ? new Runaway() : new ApproachAndAttack(), MathUtils.random(3, 5)), fireGrimoire),
                new Persona(Demography.MINOTAUR, LevelType.DUNGEON, 3 * getMaxHp() / 6, new WanderSeekApproach(false), null),
                new Persona(Demography.SKELETON_LICH, LevelType.CRYPT, 2 * getMaxHp() / 6, new Summoner(new StayAtRangeAndShoot(new Runaway(), 4), 8, 4, Demography.SKELETON, level - 2), iceGrimoire),
                new Persona(Demography.DARK_KNIGHT, LevelType.CAVE, getMaxHp() / 6, new DarkKnightBehaviour(new WanderSeekApproach()), null),
                new Persona(playerHero.getDemography(), LevelType.DEATH, 1, new WanderSeekApproach(true), weapon),
                new Persona(Demography.DEATH, LevelType.DEATH, 0, new WanderSeekApproach(true), null),
        };
    }

    private static KindOfWeapon findBestWeapon(PlayerHero playerHero) {
        final Inventory inventory = playerHero.getInventory();
        int qualityIndex = Integer.MIN_VALUE;
        KindOfWeapon best = null;
        for (int j = 0; j < inventory.size(); j++) {
            Item i = inventory.get(j);
            if (i instanceof KindOfWeapon) {
                KindOfWeapon w = (KindOfWeapon) i;
                if (w.getQualityIndex(playerHero) > qualityIndex) {
                    best = w;
                    qualityIndex = w.getQualityIndex(playerHero);
                }
            }
        }
        return best == null ? null : best.copy();
    }

    @Override
    public void receiveHit(int damage, Character enemy) {
        super.receiveHit(damage, enemy);
        final Persona currentPersona = getCurrentPersona();
        if (getHp() < currentPersona.limitHp) {
            hp = currentPersona.limitHp;
            fullMp();
            dispell();
            this.currentPersona = (this.currentPersona + 1) % personas.length;
            final Item itemToEquip = getCurrentPersona().equippedItem;
            if (itemToEquip == null) {
                disarm();
            } else {
                equip(itemToEquip);
            }
            GameEvent.deathChangedPersona();
        }
    }

    @Override
    public Demography getDemography() {
        return getCurrentPersona().demography;
    }

    @Override
    public Behaviour getAi() {
        return getCurrentPersona().behaviour;
    }

    public LevelType getLevelType() {
        return getCurrentPersona().levelType;
    }

    private Persona getCurrentPersona() {
        return personas[currentPersona];
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        GameEvent.endGame((PlayerHero) killer);
        return true;
    }

    private static class Persona {
        public final Demography demography;
        public final LevelType levelType;
        public final int limitHp;
        public final Behaviour behaviour;
        public final Item equippedItem;

        private Persona(Demography demography, LevelType levelType, int limitHp, Behaviour behaviour, Item equippedItem) {
            this.demography = demography;
            this.levelType = levelType;
            this.limitHp = limitHp;
            this.behaviour = behaviour;
            this.equippedItem = equippedItem;
        }
    }
}