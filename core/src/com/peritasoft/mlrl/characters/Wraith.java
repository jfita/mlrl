/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.ApproachAndAttack;
import com.peritasoft.mlrl.ai.WanderSeekApproach;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;

public class Wraith extends Character {
    public Wraith(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public Wraith(int level, Position pos, Inventory inventory) {
        super(Demography.WRAITH, level, 0, 0, 0, 0, 7, pos.getX(), pos.getY(),
                new WanderSeekApproach(true, new ApproachAndAttack(true)), inventory);
        this.setStr(Math.max(level / 2, 2));
        this.setDex(((int) (level * 1.25)));
        this.setWis(level / 3);
        this.setCon(Math.max(level, 2));
        this.resetHp();
    }

    @Override
    protected boolean canWalkOn(Cell cell) {
        return true;
    }

    @Override
    protected boolean inLineOfSight(Level level, Character character) {
        return true;
    }
}
