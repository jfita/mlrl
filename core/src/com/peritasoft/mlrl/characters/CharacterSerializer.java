/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.KindOfWeapon;

public abstract class CharacterSerializer<T extends Character> implements Json.Serializer<T> {

    @Override
    public void write(Json json, T object, Class knownType) {
        final KindOfWeapon equippedWeapon = object.getEquippedWeapon();
        object.unequip(equippedWeapon);
        json.writeArrayStart();
        json.writeValue(object.getHp());
        json.writeValue(object.getMp());
        json.writeValue(object.getLevel());
        json.writeValue(object.getInventory());
        json.writeValue(object.getInventory().getIndex(equippedWeapon));
        json.writeValue(object.getPosition());
        json.writeValue(object.getPrevPosition());
        json.writeValue(object.getLastDirection());
        json.writeValue(object.updated);
        json.writeValue(object.petrified);
        json.writeValue(object.confused);
        json.writeValue(object.frozen);
        json.writeValue(object.manaRegenTurn);
        json.writeValue(object.regenHp);
        writeOwnProperties(json, object);
        json.writeArrayEnd();
        object.equip(equippedWeapon);
    }

    protected void writeOwnProperties(Json json, T object) {
        // Nothing to do for Character.
    }

    @Override
    public T read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        final int hp = iter.next().asInt();
        final int mp = iter.next().asInt();
        final int level = iter.next().asInt();
        final Inventory inventory = json.readValue(Inventory.class, iter.next());
        final int equippedWeaponIndex = iter.next().asInt();
        final Position position = json.readValue(Position.class, iter.next());
        final Position prevPosition = json.readValue(Position.class, iter.next());
        final Direction lastDirection = json.readValue(Direction.class, iter.next());
        final boolean updated = iter.next().asBoolean();
        final int petrified = iter.next().asInt();
        final int confused = iter.next().asInt();
        final int frozen = iter.next().asInt();
        final int manaRegenTurn = iter.next().asInt();
        final boolean regenHp = iter.next().asBoolean();

        T object = createInstance(json, iter, level, position, inventory);
        if (equippedWeaponIndex >= 0) {
            object.equip(inventory.get(equippedWeaponIndex));
        }
        object.getPrevPosition().set(prevPosition);
        object.setLastDirection(lastDirection);

        object.hp = hp;
        object.mp = mp;
        object.updated = updated;
        object.petrified = petrified;
        object.confused = confused;
        object.frozen = frozen;
        object.manaRegenTurn = manaRegenTurn;
        object.regenHp = regenHp;

        return object;
    }

    protected abstract T createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory);
}
