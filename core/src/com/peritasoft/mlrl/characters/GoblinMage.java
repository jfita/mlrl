/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.ApproachAndAttack;
import com.peritasoft.mlrl.ai.PotionUser;
import com.peritasoft.mlrl.ai.Runaway;
import com.peritasoft.mlrl.ai.StayAtRangeAndShoot;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Potion;
import com.peritasoft.mlrl.item.PotionType;
import com.peritasoft.mlrl.weapons.WeaponGenerator;

public class GoblinMage extends Goblin {
    public GoblinMage(int level, Position pos) {
        this(level, pos, new Inventory());
        getInventory().add(WeaponGenerator.generateFireGrimoire(level - 2));
        equip(getInventory().get(0));
        getInventory().add(new Potion(PotionType.HEALTH));
    }

    public GoblinMage(int level, Position pos, Inventory inventory) {
        super(Demography.GOBLIN_MAGE, level, pos,
                new PotionUser(new StayAtRangeAndShoot(MathUtils.randomBoolean() ? new Runaway() : new ApproachAndAttack(), MathUtils.random(3, 5))),
                inventory);
        //attributes
        this.setStr(Math.max(((int) (level * 0.25)), 0));
        this.setDex(Math.max(((int) (level * 0.25)), 0));
        this.setWis(level);
        this.setCon(Math.max(((int) (level * 0.75)), 2));
        this.resetHp();
        this.fullMp();
    }
}
