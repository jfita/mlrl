/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.ApproachAndAttack;
import com.peritasoft.mlrl.ai.Behaviour;
import com.peritasoft.mlrl.ai.WeaponWielder;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.LifeTurnObj;
import com.peritasoft.mlrl.effects.Rests;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;

public class Skeleton extends Character {
    boolean raise;

    public Skeleton(Demography demography, int level, Position pos, Behaviour ai, boolean givesXP, Inventory inventory) {
        super(demography, level, 0, 0, 0, 0, 6, pos.getX(), pos.getY(), new WeaponWielder(ai), inventory);
        this.givesXP = givesXP;
        this.raise = true;
    }

    public Skeleton(int level, Position pos, boolean givesXP) {
        this(level, pos, givesXP, new Inventory());
    }

    public Skeleton(int level, Position pos, boolean givesXP, Inventory inventory) {
        this(Demography.SKELETON, level, pos, new ApproachAndAttack(), givesXP, inventory);
        this.setStr(level);
        this.setDex(level / 3);
        this.setWis(level / 4);
        this.setCon((int) (level * 1.5f));
        this.resetHp();
    }

    protected boolean canRaise() {
        return raise;
    }

    protected void cannotRaiseAgain() {
        raise = false;
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        dropItems(level);
        if (canRaise()) {
            level.add(new Rests(getPosition(), 4, this, level));
        } else {
            level.add(new Rests(getPosition(), LifeTurnObj.INFINITE));
        }
        return true;
    }

    @Override
    public boolean raise(Level level) {
        resetHp();
        setGivesXP(false);
        if (level.hasCharacterIn(getPosition())) return false;
        level.addEnemy(this);
        cannotRaiseAgain();
        if (level.getCell(getPosition()).hasItem()) {
            getInventory().add(level.getCell(getPosition()).pickupItem(this));
        }
        GameEvent.raised(this);
        return true;
    }
}
