/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.Behaviour;
import com.peritasoft.mlrl.dungeongen.*;
import com.peritasoft.mlrl.effects.Crosshairs;
import com.peritasoft.mlrl.effects.DeadRests;
import com.peritasoft.mlrl.effects.Projectile;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Arrow;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.KindOfWeapon;
import com.peritasoft.mlrl.weapons.Fist;
import com.peritasoft.mlrl.weapons.Shootable;
import com.peritasoft.mlrl.weapons.Weapon;

import java.util.Iterator;

public abstract class Character {
    protected Demography demography;
    private int level;
    private int str;
    private int dex;
    private int wis;
    private int con;
    int hp;
    int mp;
    private final int sightRadius;
    protected final Inventory inventory;
    private Behaviour ai;
    boolean updated;
    int petrified;
    int confused;
    int frozen;
    private float attackTime = 0f;

    private final Position position;
    private final Position prevPosition;
    private Direction lastDirection;

    private final Fist fist;
    private Weapon weapon;

    int manaRegenTurn;
    boolean regenHp;
    boolean givesXP;

    Character(Demography demography, int level, int str, int dex, int wis, int con,
              int sightRadius, int positionX, int positionY, Inventory inventory) {
        this(demography, level, str, dex, wis, con, sightRadius, positionX, positionY, null, inventory);
    }

    Character(Demography demography, int level, int str, int dex, int wis, int con,
              int sightRadius, int positionX, int positionY, Behaviour ai, Inventory inventory) {
        this.demography = demography;
        this.level = level;
        this.str = str;
        this.dex = dex;
        this.wis = wis;
        this.con = con;
        this.hp = con * 2;
        this.mp = wis * 5;
        this.sightRadius = sightRadius;
        this.position = new Position(positionX, positionY);
        this.prevPosition = new Position(positionX, positionY);
        this.lastDirection = Direction.SOUTH;
        this.updated = false;
        this.ai = ai;
        this.fist = new Fist();
        this.petrified = 0;
        this.confused = 0;
        this.frozen = 0;
        this.inventory = inventory;
        this.manaRegenTurn = 0;
        this.regenHp = true;
        this.givesXP = true;
    }


    public Demography getDemography() {
        return demography;
    }

    public Race getRace() {
        return demography.race;
    }

    public Klass getKlass() {
        return demography.klass;
    }

    public String getName() {
        return demography.name;
    }

    public int getLevel() {
        return level;
    }

    public int getStr() {
        return str;
    }

    public int getDex() {
        return dex;
    }

    public int getWis() {
        return wis;
    }

    public int getCon() {
        return con;
    }

    public int getSightRadius() {
        return sightRadius;
    }

    public Position getPosition() {
        return position;
    }

    public int getPositionX() {
        return position.getX();
    }

    public int getPositionY() {
        return position.getY();
    }

    public Position getPrevPosition() {
        return prevPosition;
    }

    public Direction getLastDirection() {
        return lastDirection;
    }

    public Behaviour getAi() {
        return ai;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public void setLastDirection(Direction direction) {
        // So that we do not need to handle “no direction” when rendering.
        if (direction == Direction.NONE) return;
        this.lastDirection = direction;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public void setWis(int wis) {
        this.wis = wis;
    }

    public void setCon(int con) {
        this.con = con;
    }

    public void incStr(int inc) {
        this.str += inc;
    }

    public void decStr(int dec) {
        this.str -= dec;
    }

    public void incDex(int inc) {
        this.dex += inc;
    }

    public void decDex(int dec) {
        this.dex -= dec;
    }

    public void incWis(int inc) {
        final float p = getMaxMp() == 0 ? 0f : (float) getMp() / (float) getMaxMp();
        this.wis += inc;
        this.mp = Math.max(0, (int) (getMaxMp() * p + 0.5f));
    }

    public void decWis(int dec) {
        final float p = getMaxMp() == 0 ? 0f : (float) getMp() / (float) getMaxMp();
        this.wis -= dec;
        this.mp = Math.max(0, (int) (getMaxMp() * p + 0.5f));
    }

    public void incCon(int inc) {
        final float p = (float) getHp() / (float) getMaxHp();
        this.con += inc;
        this.hp = Math.max(1, (int) (getMaxHp() * p + 0.5f));
    }

    public void decCon(int dec) {
        final float p = (float) getHp() / (float) getMaxHp();
        this.con -= dec;
        this.hp = Math.max(1, (int) (getMaxHp() * p + 0.5f));
    }

    public boolean canRegenHp() {
        return regenHp;
    }

    public void setRegenHp(boolean regenHp) {
        this.regenHp = regenHp;
    }

    public boolean doesGiveXP() {
        return givesXP;
    }

    public void setGivesXP(boolean givesXP) {
        this.givesXP = givesXP;
    }

    public void resetHp() {
        hp = getMaxHp();
    }

    public void fullHeal() {
        heal(getMaxHp() - hp);
    }

    public void heal(int toHeal) {
        if (toHeal > 0) {
            int healed = Math.min(hp + toHeal, getMaxHp()) - hp;
            hp += healed;
            GameEvent.healed(this, healed);
        }
    }

    public void fullMp() {
        mp = getMaxMp();
    }

    public void restoreMp(int mpRestored) {
        if (mp + mpRestored > getMaxMp()) {
            fullMp();
        } else {
            mp += mpRestored;
        }
    }

    public int getMaxHp() {
        return con * 2;
    }

    public int getMaxMp() {
        return Math.max(wis * 5, 0);
    }

    public void setPosition(Position position, Level level) {
        this.setPosition(position.getX(), position.getY(), level);
    }

    public void setAi(Behaviour ai) {
        this.ai = ai;
    }

    public boolean isDead() {
        return hp <= 0;
    }

    protected void setPosition(int positionX, int positionY, Level level) {
        level.move(this, getPosition(), positionX, positionY);
        this.position.set(positionX, positionY);
    }

    protected void manaRegen() {
        manaRegenTurn += 1;
        if (manaRegenTurn == 10) {
            manaRegenTurn = 0;
            if (mp < getMaxMp()) mp += 1;
        }
    }

    public Action update(Level level) {
        manaRegen();
        return getAi().update(level, this, findClosestVictim(level));
    }

    public void update(float delta) {
        attackTime -= delta;
    }

    public boolean isAttacking() {
        return attackTime > 0f;
    }

    protected Character findClosestVictim(final Level level) {
        Character victim = null;
        int maxDistance = Integer.MAX_VALUE;
        for (int y = -sightRadius; y <= sightRadius; y++) {
            final int row = getPositionY() + y;
            for (int x = -sightRadius; x <= sightRadius; x++) {
                final int col = getPositionX() + x;
                Cell cell = level.getCell(col, row);
                final Character character = cell.getCharacter();
                if (character != null && canAttack(character) && inLineOfSight(level, character)) {
                    final int distance = character.getPosition().distance(getPosition());
                    if (distance < maxDistance) {
                        maxDistance = distance;
                        victim = character;
                    }
                }
            }
        }
        return victim;
    }

    protected boolean inLineOfSight(final Level level, Character character) {
        return Geometry.walkLine(getPosition(), character.getPosition(), new Geometry.LineWalker() {
            @Override
            public boolean walk(Position p) {
                return !level.blockSight(p.getX(), p.getY());
            }
        });
    }

    public Action move(Direction direction, Level level) {
        if (isPetrified() || isFrozen()) {
            return skipTurn();
        }
        setLastDirection(direction);
        return move(getPositionX() + direction.x, getPositionY() + direction.y, level);
    }

    public boolean isPetrified() {
        return petrified > 0;
    }

    protected Action skipTurn() {
        if (petrified > 0) {
            petrified--;
            GameEvent.skipPetrifiedTurn(this, petrified);
        } else if (frozen > 0) {
            frozen--;
            GameEvent.skipFrozenTurn(this, frozen);
        }
        return Action.SKIP_TURN;
    }

    protected Action move(final int x, final int y, final Level level) {
        prevPosition.set(getPositionX(), getPositionY());
        final Cell cell = level.getCell(x, y);
        if (canWalkOn(cell)) {
            Character other = cell.getCharacter();
            if (canAttack(other)) {
                attack(other, level);
                return Action.ATTACK;
            }
            if (other == null) {
                setPosition(x, y, level);
                Item item = cell.pickupItem(this);
                if (item == null) {
                    if (this instanceof PlayerHero) GameEvent.playerMoves();
                    return Action.MOVE;
                } else if (getInventory().add(item)) {
                    GameEvent.pickedUp(this, item);
                } else {
                    cell.setItem(item);
                    inventoryFull();
                }
                return Action.PICK_UP_ITEM;
            }
        }
        if (openProp(cell, level)) {
            return Action.OPEN_PROP;
        }
        return Action.COLLIDE;
    }

    protected boolean canWalkOn(Cell cell) {
        return cell.isWalkable();
    }

    protected void inventoryFull() {

    }

    public boolean canAttack(Character other) {
        return other != null &&
                ((getRace() == Race.HUMAN && other.getRace() != Race.HUMAN) ||
                        (getRace() != Race.HUMAN && other.getRace() == Race.HUMAN));
    }

    protected boolean openProp(Cell cell, Level level) {
        return false;
    }

    public void attack(Character obj, Level level) {
        Character victim = obj;
        final int strModulation = 4;
        final int prevStr = this.str;
        if (isConfused()) {
            confused--;
            if (MathUtils.randomBoolean()) {
                victim = this;
                this.str /= strModulation;
            }
        }
        attackWeapon().attack(this, victim, level);
        this.str = prevStr;
        attackTime = 0.25f;
    }

    public boolean isConfused() {
        return confused > 0;
    }

    public boolean isFrozen() {
        return frozen > 0;
    }

    Weapon attackWeapon() {
        return weapon != null ? weapon : fist;
    }

    public Shootable shootWeapon() {
        if (weapon instanceof Shootable) return (Shootable) weapon;
        return null;
    }

    public void receiveHit(int damage, Character enemy) {
        hp -= damage;
        if (MathUtils.randomBoolean(0.05f)) {
            confuse();
        }
    }

    public void spendMana(int mana) {
        mp -= mana;
    }

    public int getHp() {
        return hp;
    }

    public int getMp() {
        return mp;
    }

    public boolean canFireWeapon() {
        Shootable w = shootWeapon();
        return w != null && w.canShoot(this) && !isPetrified();
    }

    public int getFireRange() {
        return shootWeapon().getRange();
    }

    public void equip(Item item) {
        if (!item.isEquipable()) {
            return;
        }
        if (this.weapon != null) {
            this.weapon.onUnequiped(this);
        }
        if (this.weapon == item) {
            this.weapon = null;
        } else {
            this.weapon = (Weapon) item;
            this.weapon.onEquiped(this);
        }
    }

    public void unequip(Item item) {
        if (getEquippedWeapon() == item) {
            equip(item);
        }
    }

    public void disarm() {
        this.weapon = null;
    }

    public Item giveEquipedWeapon() {
        if (weapon != null) {
            Item item = (Item) weapon;
            this.weapon.onUnequiped(this);
            getInventory().remove((Item) weapon);
            weapon = null;
            return item;
        }
        return null;
    }

    public boolean use(Item item, Level level) {
        if (!item.isUsable()) {
            return false;
        }
        item.use(this, level);
        return true;
    }

    public int distance(Character other) {
        return getPosition().distance(other.getPosition());
    }

    public Inventory getInventory() {
        return inventory;
    }

    public KindOfWeapon getEquippedWeapon() {
        if (weapon == null) return fist;
        return (KindOfWeapon) weapon;
    }

    public int countArrows() {
        return inventory.countArrows();
    }

    public void grabArrows(int arrows) {
        inventory.putArrows(arrows);
    }

    public void shootArrow() {
        inventory.takeArrows(1);
    }

    public int petrify() {
        this.petrified = MathUtils.random(2, 4);
        return this.petrified;
    }

    public void confuse() {
        if (!isConfused()) {
            this.confused = MathUtils.random(2, 4);
            GameEvent.confused(this);
        }
    }

    public void freeze(int turnsFrozen) {
        this.frozen = turnsFrozen;
    }

    public void dispell() {
        this.frozen = 0;
        this.confused = 0;
    }

    public void aim(Shootable shootable, Level level) {
        // Nothing to do in general
    }

    public boolean shoot(Level level, Crosshairs xh) {
        Projectile projectile = xh.shoot(this, level);
        if (projectile == null) {
            return false;
        }
        level.setProjectile(projectile);
        setLastDirection(angleToDirection(projectile.getAngle()));
        return true;
    }

    private Direction angleToDirection(float angle) {
        if (angle >= 45 && angle < 135) {
            return Direction.NORTH;
        }
        if (angle >= 135 && angle < 225) {
            return Direction.WEST;
        }
        if (angle >= 225 && angle <= 315) {
            return Direction.SOUTH;
        }
        return Direction.EAST;
    }

    public void resetPrevPosition() {
        prevPosition.set(position);
    }

    public boolean onKilled(Level level, Character killer) {
        level.add(new DeadRests(getPosition(), 3));
        dropItems(level);
        return true;
    }

    public int getXp(float playerLevel) {
        if (!givesXP) {
            return 0;
        }
        return (int) (10 * ((float) getLevel() / playerLevel));
    }


    protected void dropItems(Level level) {
        dropItems(level, 0.40f);
    }

    protected void dropItems(Level level, float chance) {
        weapon = null;
        final Inventory items = getInventory();
        final Iterator<Item> iterator = items.iterator();
        while (iterator.hasNext()) {
            final Item item = iterator.next();
            if (MathUtils.randomBoolean(chance)) {
                level.dropItem(item, getPosition());
                iterator.remove();
            }
        }
        if (MathUtils.randomBoolean(chance)) {
            final int numArrows = MathUtils.random((int) (items.countArrows() * 0.4f));
            if (numArrows > 0) {
                level.dropItem(new Arrow(numArrows), getPosition());
            }
        }
    }

    public boolean shouldPickup(Item item) {
        return item instanceof Arrow && !getInventory().hasABow() ? false : true;
    }

    public boolean isFlamable() {
        return true;
    }

    public boolean isAtMelee(Character enemy) {
        return (getPositionY() == enemy.getPositionY() && (getPositionX() + 1 == enemy.getPositionX() || getPositionX() - 1 == enemy.getPositionX())) ||
                (getPositionX() == enemy.getPositionX() && (getPositionY() + 1 == enemy.getPositionY() || getPositionY() - 1 == enemy.getPositionY()));
    }

    public boolean raise(Level level) {
        return false;
    }
}
