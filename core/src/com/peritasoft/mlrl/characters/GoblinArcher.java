/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.ApproachAndAttack;
import com.peritasoft.mlrl.ai.StayAtRangeAndShoot;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.weapons.WeaponGenerator;

public class GoblinArcher extends Goblin {
    public GoblinArcher(int level, Position pos) {
        this(level, pos, new Inventory());
        getInventory().add(WeaponGenerator.generateBow(level - 1));
        equip(this.getInventory().get(0));
        grabArrows(10);
    }

    public GoblinArcher(int level, Position pos, Inventory inventory) {
        super(Demography.GOBLIN_ARCHER, level, pos, new StayAtRangeAndShoot(new ApproachAndAttack(), 4), inventory);
        //attributes
        this.setStr(level / 2);
        this.setDex((int) Math.max(level * 0.75f, 2));
        this.setWis(level / 4);
        this.setCon(Math.max(level, 3));
        this.resetHp();
    }
}
