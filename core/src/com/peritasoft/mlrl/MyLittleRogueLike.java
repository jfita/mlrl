/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.mlrl.characters.Sex;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.scores.LocalScoreRecorder;
import com.peritasoft.mlrl.scores.RemoteScoreRecorder;
import com.peritasoft.mlrl.scores.Score;
import com.peritasoft.mlrl.screens.MlrlScreen;
import com.peritasoft.mlrl.screens.PeritaSoftScreen;
import com.peritasoft.mlrl.screens.PlayScreen;
import com.peritasoft.mlrl.screens.TitleScreen;
import com.peritasoft.mlrl.serialization.MyLittleSerializer;

import java.util.List;

public class MyLittleRogueLike implements ApplicationListener, InputProcessor {
    public static final int SCREENWIDTH = 800 / 2;
    public static final int SCREENHEIGHT = 480 / 2;
    public static float keyRepeatInitialTime = 0.4f;
    public static float keyRepeatTime = 0.1f;

    private static final String CHOICE_SEX_KEY = "player-choice-sex";
    private static final String CHOICE_OPTION_KEY = "player-choice-option";
    private static final String PLAY_MUSIC_KEY = "play-music";
    private static final String SFX_ENABLE_KEY = "enable-sfx";
    private static final String USE_GRAPHICS_KEY = "use-graphics";
    private static final String SHOW_PAD_KEY = "show-pad";
    private static final String RIGHT_PAD_KEY = "right-pad";
    private static final String RUN_TIME_KEY = "run-time";
    private static final String ONGOING_RUN_KEY = "ongoing-run";

    protected MlrlScreen screen;
    private Preferences preferences;
    private SpriteBatch batch;
    private BitmapFont textFont;
    private BitmapFont smallFont;
    private KeyRepeatTask keyRepeatTask;
    protected Viewport viewport;
    protected final Vector2 touchPoint = new Vector2();
    private final Config config;
    private LocalScoreRecorder localScoreRecorder;
    protected Json json;

    public MyLittleRogueLike(Config config) {
        this.config = config;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        textFont = new BitmapFont();
        smallFont = new BitmapFont(Gdx.files.internal("terminal8x8_aa_ro.fnt"));
        viewport = new FitViewport(MyLittleRogueLike.SCREENWIDTH, MyLittleRogueLike.SCREENHEIGHT);
        keyRepeatTask = new KeyRepeatTask();
        preferences = Gdx.app.getPreferences("mlrl");
        GameEvent.register(config.remoteScoreRecorder);
        Gdx.input.setInputProcessor(this);
        config.playMusic = preferences.getBoolean(PLAY_MUSIC_KEY, config.playMusic);
        config.enableSfx = preferences.getBoolean(SFX_ENABLE_KEY, config.enableSfx);
        config.useGraphics = preferences.getBoolean(USE_GRAPHICS_KEY, config.useGraphics);
        config.showPad = preferences.getBoolean(SHOW_PAD_KEY, config.showPad);
        config.rightPad = preferences.getBoolean(RIGHT_PAD_KEY, config.rightPad);

        json = new Json();
        MyLittleSerializer.registerClasses(this, json);
        localScoreRecorder = new LocalScoreRecorder(preferences, json);
        GameEvent.register(localScoreRecorder);

        setScreen(startScreen());
    }

    protected MlrlScreen startScreen() {
        return new PeritaSoftScreen(this);
    }

    public boolean canResumeGame() {
        return preferences.contains(ONGOING_RUN_KEY);
    }

    public void resumeGame() {
        final long prevRunTime = preferences.getLong(RUN_TIME_KEY, TimeUtils.millis());
        localScoreRecorder.setRunTime(prevRunTime);
        if (config.remoteScoreRecorder != null) {
            config.remoteScoreRecorder.setRunTime(prevRunTime);
        }
        final String ongoingRun = preferences.getString(ONGOING_RUN_KEY);
        if (ongoingRun != null) {
            preferences.remove(ONGOING_RUN_KEY);
            preferences.flush();
            try {
                final PlayScreen playScreen = json.fromJson(PlayScreen.class, ongoingRun);
                // playScreen **could** be null if the serialization is an empty object, for instance
                if (playScreen != null) {
                    setScreen(playScreen);
                    return;
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                // fall back to the default start screen
            }
        }
        setScreen(new TitleScreen(this));
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public BitmapFont getTextFont() {
        return textFont;
    }

    public BitmapFont getSmallFont() {
        return smallFont;
    }

    public Viewport getViewport() {
        return viewport;
    }

    @Override
    public void dispose() {
        if (screen != null) screen.hide();
        if (screen != null) screen.dispose();
        textFont.dispose();
        batch.dispose();
        preferences.flush();
    }

    @Override
    public void pause() {
        if (screen != null) screen.pause();
    }

    @Override
    public void resume() {
        if (screen != null) screen.resume();
    }

    @Override
    public void render() {
        if (screen != null) {
            ScreenUtils.clear(0, 0, 0, 1);
            viewport.apply(true);
            batch.setProjectionMatrix(viewport.getCamera().combined);
            screen.render(Gdx.graphics.getDeltaTime());
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        if (screen != null) screen.resize(width, height);
    }

    /**
     * Sets the current screen. {@link Screen#hide()} is called on any old screen, and {@link Screen#show()} is called on the new
     * screen, if any.
     *
     * @param screen may be {@code null}
     */
    public void setScreen(MlrlScreen screen) {
        if (this.screen != null) this.screen.hide();
        this.screen = screen;
        if (this.screen != null) {
            this.screen.show();
            this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if (screen == null) {
            return false;
        }
        if (!screen.keyDown(keycode)) {
            return false;
        }
        scheduleKeyRepeatTask(keycode);
        return true;
    }

    protected void scheduleKeyRepeatTask(int keycode) {
        if (!keyRepeatTask.isScheduled() || keyRepeatTask.keycode != keycode) {
            keyRepeatTask.keycode = keycode;
            keyRepeatTask.cancel();
            Timer.schedule(keyRepeatTask, keyRepeatInitialTime, keyRepeatTime);
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        keyRepeatTask.cancel();
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (screen != null) {
            touchPoint.set(screenX, screenY);
            viewport.unproject(touchPoint);
            return screen.touchDown(touchPoint, pointer, button);
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (screen != null) {
            touchPoint.set(screenX, screenY);
            viewport.unproject(touchPoint);
            return screen.touchUp(touchPoint, pointer, button);
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (screen != null) {
            touchPoint.set(screenX, screenY);
            viewport.unproject(touchPoint);
            return screen.touchDragged(touchPoint, pointer);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }

    public boolean shouldShowPad() {
        return config.showPad;
    }

    public boolean isPadAtRight() {
        return config.rightPad;
    }

    public boolean isMusicOn() {
        return config.playMusic;
    }

    public boolean isSfxEnabled() {
        return config.enableSfx;
    }

    public boolean isTilesetGraphical() {
        return config.useGraphics;
    }

    public void setIsMusicOn(boolean playing) {
        config.playMusic = playing;
        preferences.putBoolean(PLAY_MUSIC_KEY, playing);
        preferences.flush();
    }

    public void setIsSfxOn(boolean enabled) {
        config.enableSfx = enabled;
        preferences.putBoolean(SFX_ENABLE_KEY, enabled);
        preferences.flush();
    }

    public void setIsTilesetGraphical(boolean atlasGraphical) {
        config.useGraphics = atlasGraphical;
        preferences.putBoolean(USE_GRAPHICS_KEY, atlasGraphical);
        preferences.flush();
    }

    public void setShouldShodPad(boolean showPad) {
        config.showPad = showPad;
        preferences.putBoolean(SHOW_PAD_KEY, showPad);
        preferences.flush();
    }

    public void setRightPad(boolean rightPad) {
        config.rightPad = rightPad;
        preferences.putBoolean(RIGHT_PAD_KEY, rightPad);
        preferences.flush();
    }

    public LocalScoreRecorder getLocalScoreRecorder() {
        return localScoreRecorder;
    }

    public void recordPlayerChoice(int selection, Sex sex) {
        preferences.putInteger(CHOICE_OPTION_KEY, selection);
        preferences.putString(CHOICE_SEX_KEY, sex.name());
        preferences.flush();
    }

    public Sex getLastSexChoice(Sex defaultSex) {
        return Sex.valueOf(preferences.getString(CHOICE_SEX_KEY, defaultSex.name()));
    }

    public int getLastPlayerChoice(int defaultChoice) {
        return preferences.getInteger(CHOICE_OPTION_KEY, defaultChoice);
    }

    public void saveOngoingRun(Object run) {
        preferences
                .putLong(RUN_TIME_KEY, localScoreRecorder.getRunTime())
                .putString(ONGOING_RUN_KEY, json.toJson(run))
                .flush();
    }

    public void clearOngoingRun() {
        preferences.remove(ONGOING_RUN_KEY);
        preferences.flush();
    }

    public boolean isArcherUnlocked() {
        return localScoreRecorder.isArcherUnlocked();
    }

    public boolean isMageUnlocked() {
        return localScoreRecorder.isMageUnlocked();
    }

    public List<Score> getScoreList() {
        return localScoreRecorder.getHighScores();
    }

    public boolean isEndlessUnlocked() {
        return localScoreRecorder.isEndlessUnlocked();
    }

    class KeyRepeatTask extends Timer.Task {
        int keycode;

        public void run() {
            keyDown(keycode);
        }
    }

    public static class Config {
        public boolean showPad = false;
        public RemoteScoreRecorder remoteScoreRecorder = null;
        public boolean useGraphics = true;
        public boolean playMusic = true;
        public boolean enableSfx = true;
        public boolean rightPad = true;
    }
}
