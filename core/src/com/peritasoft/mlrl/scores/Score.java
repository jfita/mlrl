/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.scores;

import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Sex;

public class Score implements Comparable<Score> {
    public final Sex sex;
    public final Demography player;
    public final String playerName;
    public final Demography killer;
    public final int floor;
    public final String dateTime;
    public final long runTime;
    public final String runTimeString;

    public Score(PlayerHero playerHero, int floor, String dateTime, long runTime) {
        this(playerHero.getSex(), playerHero.getDemography(),
                playerHero.getKiller() == null ? null : playerHero.getKiller().getDemography(), floor, dateTime, runTime);
    }

    public Score(Sex sex, Demography player, Demography killer, int floor, String dateTime, long runTime) {
        this.sex = sex;
        this.player = player;
        this.playerName = player.getNameBySex(sex);
        this.killer = killer;
        this.floor = floor;
        this.dateTime = dateTime;
        this.runTime = runTime;
        this.runTimeString = formatRunTime(runTime);
    }

    @Override
    public int compareTo(Score score) {
        return dateTime.compareTo(score.dateTime);
    }

    private String formatRunTime(long runTime) {
        int seconds = (int) (runTime / 1000f) % 60;
        int minutes = (int) (runTime / 60000f);
        return formatTime(minutes) + ":" + formatTime(seconds);
    }

    private String formatTime(int time) {
        return time < 10 ? "0" + time : String.valueOf(time);
    }

}
