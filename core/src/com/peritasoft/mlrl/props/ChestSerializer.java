/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.props;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public abstract class ChestSerializer<T extends Chest> implements Json.Serializer<T> {
    @Override
    public void write(Json json, T object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.isOpen);
        writeOwnProperties(json, object);
        json.writeArrayEnd();
    }

    protected abstract void writeOwnProperties(Json json, T object);

    @Override
    public T read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        final boolean isOpen = iter.next().asBoolean();
        final T chest = createInstance(json, iter);
        chest.isOpen = isOpen;
        return chest;
    }

    protected abstract T createInstance(Json json, JsonValue.JsonIterator iter);
}
