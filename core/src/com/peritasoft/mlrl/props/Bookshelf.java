/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.props;

import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemGenerator;

public class Bookshelf implements Prop {
    boolean isOpen;
    private Item item;

    public Bookshelf(int level) {
        this(ItemGenerator.generateScroll(level));
    }

    public Bookshelf(Item item) {
        this.item = item;
    }

    public Item getItem() {
        return item;
    }

    @Override
    public PropType getType() {
        return isOpen ? PropType.BOOKSHELF_EMPTY : PropType.BOOKSHELF_FULL;
    }

    @Override
    public boolean isWalkable() {
        return false;
    }

    @Override
    public boolean blocksLight() {
        return true;
    }

    @Override
    public void open(PlayerHero playerHero, Level level) {
        if (isOpen) {
            return;
        }
        if (playerHero.getInventory().add(item)) {
            isOpen = true;
            GameEvent.inspectBookshelf(item);
        } else {
            playerHero.inventoryFull();
        }

    }
}
