/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class Lance extends MeleeWeapon implements Weapon {

    public Lance(int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
        super("Lance", "A simply lance", bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
        this.description = getRandomDescription();
    }

    @Override
    public Lance copy() {
        return new Lance(getBonusStr(), getBonusDex(), getBonusCon(), getBonusWis(), getBonusDamage());
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() >
                MathUtils.random(1, 20) + target.getDex()) {
            int dmg = attacker.getStr() + rollBonusDamage();
            GameEvent.attackHit(attacker, target, dmg, getCategory());
            target.receiveHit(dmg, attacker);
        } else {
            GameEvent.attackMissed(attacker, target);
        }
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.WEAPON_QUARTERSTAFF;
    }

    private String getRandomDescription() {
        String[] desc = {"The serrated, very sharp head is crudely bound to the shaft with old leather strips.",
                "The leaf-shaped, rough head is bound to the shaft with a series of slots and teeth.",
                "It was created as a decoration, is very old, but appears new and once belonged to an ancient royal.",
                "The leaf-shaped, sharp head is securely bound to the shaft with a highly decorated metal sleeve.",
                "An iron headed javelin created for combat, is very old, but appears new and is a ceremonial weapon.",
                "A silver headed jabbing spear. The diamond-shaped, chipped head is weakly bound to the shaft with neat leather strips.",
                "A bronze headed spear. The rounded, blunt head is weakly bound to the shaft with a highly decorated metal sleeve.",
                "A steel headed jabbing spear. The dual pointed, jagged head is bound to the shaft with an engraved metal sleeve."
        };
        return desc[MathUtils.random(desc.length - 1)];
    }
}
