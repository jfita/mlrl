/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class Fist extends MeleeWeapon implements Weapon {

    public Fist() {
        super("Fist of Doom", "A pair of powerful fists to bring the doom to this world.",
                0, 0, 0, 0, 0);
    }

    @Override
    public Fist copy() {
        return new Fist();
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() > MathUtils.random(1, 20) + target.getDex()) {
            GameEvent.attackHit(attacker, target, attacker.getStr(), getCategory());
            target.receiveHit(attacker.getStr(), attacker);
        } else {
            GameEvent.attackMissed(attacker, target);
        }
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.FIST;
    }
}
