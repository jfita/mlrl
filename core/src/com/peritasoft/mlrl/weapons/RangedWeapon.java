/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.item.KindOfWeapon;

public abstract class RangedWeapon extends KindOfWeapon implements Shootable {
    public RangedWeapon(String name, String description, int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
        super(name, description, bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
    }

    @Override
    public int getQualityIndex(Character wielder) {
        if (!canShoot(wielder)) return -1;
        return ((getBonusStr() + getBonusDex()) / 2) + getBonusDamage();
    }
}
