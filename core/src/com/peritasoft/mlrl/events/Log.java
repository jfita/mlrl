/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.events;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Sex;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.item.Potion;
import com.peritasoft.mlrl.item.PotionVariations;

import java.util.ArrayList;
import java.util.List;

public class Log implements GameEventListener, Disposable {
    private final List<Line> messages = new ArrayList<>();
    private final PlayerHero player;
    private final String zie;
    private final String zirs;

    public Log(PlayerHero player) {
        this.player = player;
        if (player.getSex() == Sex.FEMALE) {
            zie = "she";
            zirs = "her";
        } else {
            zie = "he";
            zirs = "his";
        }
        GameEvent.register(this);
    }

    public Line getLine(int logLine) {
        int index = messages.size() - logLine - 1;
        if (index < 0) {
            return null;
        }
        return messages.get(index);
    }

    public int countLines() {
        return messages.size();
    }

    @Override
    public void dispose() {
        GameEvent.unregister(this);
    }

    public void add(String message) {
        add(message, Color.WHITE);
    }

    public void add(String message, Color color) {
        if (messageRepeated(message)) {
            return;
        }
        messages.add(new Line(message, color));
    }

    private boolean messageRepeated(String message) {
        if (messages.isEmpty()) {
            return false;
        }
        final Line mostRecent = getLine(0);
        if (!mostRecent.message.equals(message)) {
            return false;
        }
        mostRecent.repeated++;
        return true;
    }

    @Override
    public void attackHit(Character attacker, Character victim, int damage, ItemCategory itemCategory) {
        add(attacker.getName() + " hits " + victim.getName() + " and deals " + damage + " of damage", Color.ORANGE);
    }

    @Override
    public void attackMissed(Character attacker, Character victim) {
        add(attacker.getName() + " misses " + victim.getName(), Color.GOLD);
    }

    @Override
    public void died(PlayerHero player) {
        add(player.getName() + " is " + (player.getHp() == 0 ? "" : "very ") + "dead", Color.RED);
        add("Press Enter to start a new game");
    }

    @Override
    public void drank(Potion potion, Character drinker) {
        final String[] message =
                {", now " + zie + " feels rejuvenated.",
                        ", " + zirs + " body feels warm.",
                        ", " + zie + " feels " + zirs + " strength raising.",
                        ", " + zie + " feels faster and agile.",
                        ", " + zirs + " mind becomes clarified.",
                        ", ugh? Where are you?"};
        int msg = PotionVariations.getPotionType(potion.getColour()).ordinal();
        String potionName = player.knowsPotion(potion.getColour()) ? potion.getKnownName() : potion.getName();
        add(drinker.getName() + " drinks " + potionName + message[msg]);
    }

    @Override
    public void pickedUp(Character character, Item item) {
        String name = item instanceof Potion && player.knowsPotion(((Potion) item).getColour()) ? ((Potion) item).getKnownName() : item.getName();
        add(character.getName() + " picked up " + name);
    }

    @Override
    public void killed(Character c) {
        add(c.getName() + " dies", Color.FIREBRICK);
    }

    @Override
    public void lookedAt(Item item) {
        if (item == null) {
            add("You don't see anything interesting");
        } else {
            add("Looking around you see a " + item.getName());
        }
    }

    @Override
    public void luredByDecoy(Character enemy) {
        add(enemy.getName() + " lured you into a trap!");
    }

    @Override
    public void openedChest(Item item) {
        String name = item instanceof Potion && player.knowsPotion(((Potion) item).getColour()) ? ((Potion) item).getKnownName() : item.getName();
        add("You open the chest and find " + name);
    }

    @Override
    public void spottedPlayer(Character enemy) {
        String[] taunts = {"yelling", "screaming", "cursing", "gesturing obscenely", "pointing at you", "giving you the bird"};
        String reaction;
        switch (enemy.getRace()) {
            case RAT:
                reaction = "squeaking loudly";
                break;
            case BEETLE:
                reaction = "hissing furiously";
                break;
            case SPIDER:
                reaction = "spitting silk web";
                break;
            case BAT:
                reaction = "flying towards you";
                break;
            case SLIME:
            case JELLY:
                reaction = "creeping";
                break;
            case SNAKE:
                reaction = "spitting poison";
                break;
            case MINOTAUR:
                reaction = "stomping ground";
                break;
            case ELEMENTAL:
                reaction = "crackling";
                break;
            case WRAITH:
                reaction = "howling";
                break;
            case BEHOLDER:
                reaction = "glaring";
                break;
            case HUMAN:
            case GOBLIN:
            case SKELETON:
            case ZOMBIE:
            case MIMIC:
            case BOSS:
            case GIANT:
            case DWARF:
            case KNIGHT:
            case DEMON:
                reaction = taunts[MathUtils.random(taunts.length - 1)];
                break;
            default:
                throw new IllegalArgumentException("No taunt for: " + enemy.getRace());
        }
        add(enemy.getName() + " starts " + reaction + " and runs after you", Color.SKY);
    }

    @Override
    public void fireSpellgemCracked() {
        add("Your Spellgem cracks and the glowing fades out");
    }

    @Override
    public void levelUp(int newLevel, int strUp, int dexUp, int wisUp, int conUp) {
        add("Congratulations, you reached level " + newLevel + "!", Color.CHARTREUSE);
        String msg = "You gain";
        if (strUp != 0) msg = msg + ", +" + strUp + " STR";
        if (dexUp != 0) msg = msg + ", +" + dexUp + " DEX";
        if (wisUp != 0) msg = msg + ", +" + wisUp + " WIS";
        if (conUp != 0) msg = msg + ", +" + conUp + " CON";
        if (strUp + dexUp + wisUp + conUp > 0) {
            add(msg, Color.CHARTREUSE);
        }
    }

    @Override
    public void teleported() {
        add("You got teleported to a random position");
    }

    @Override
    public void teleported(Character blinker) {
        add(blinker.getName() + " blinks just in front of you");
    }

    @Override
    public void healed(Character character, int hpHealed) {
        add(character.getName() + " healed " + hpHealed + " HP", Color.GREEN);
    }

    @Override
    public void castFireNova() {
        //log message unnecessary
    }

    @Override
    public void castIceNova() {
        //log message unnecessary
    }

    @Override
    public void petrified(Character character, int turns) {
        add(character.getName() + " get petrified and can not move for " + turns + " turns.");
    }

    @Override
    public void confused(Character character) {
        add(character.getName() + " receives a hit to the head, now is confused");
    }

    @Override
    public void skipPetrifiedTurn(Character character, int turns) {
        if (turns == 0) {
            add(character.getName() + " is petrified and can not do anything until next turn");
        } else {
            add(character.getName() + " is petrified and can not do anything for " + turns + " more turns");
        }
    }

    @Override
    public void skipFrozenTurn(Character character, int turns) {
        if (turns == 0) {
            add(character.getName() + " is frozen and can not move until next turn");
        } else {
            add(character.getName() + " is frozen and can not move for " + turns + " more turns");
        }
    }

    @Override
    public void notEnoughWis() {
        add("Not enough Wisdom to use this grimoire");
    }

    @Override
    public void notEnoughMana(Character character) {
        add(character.getName() + " try to cast a spell but has not enough mana");
    }

    @Override
    public void attackByPoison(Character attacker, Character victim, int damage) {
        add(attacker.getName() + " poison hits " + victim.getName() + " and deals " + damage + " of damage", Color.ORANGE);
    }

    @Override
    public void shootMissed() {
        add("You miss your shoot", Color.GOLD);
    }

    @Override
    public void openedEmptyChest() {
        add("This chest seems to be empty, better luck on the next ones");
    }

    @Override
    public void raiseStat(Character character, String stat, int amountRaised) {
        add(character.getName() + " raises " + zirs + " " + stat + " by " + amountRaised);
    }

    @Override
    public void changedFloor(int floor, Level level) {
        level.logEnter(this);
    }

    @Override
    public void inventoryFull() {
        add("Your inventory is FULL");
    }

    @Override
    public void weaponEmbedded(Character attacker) {
        add("Your weapon gets stuck on " + attacker.getName() + ". Now you are disarmed!!", Color.RED);
    }

    @Override
    public void newGame(long seed) {
        add("Welcome to My Little Roguelike");
    }

    @Override
    public void pauseGame() {
        // Nothing to do
    }

    @Override
    public void resumeGame(long seed) {
        // Nothing to do
    }

    @Override
    public void summonMob(Character summoner, Character summoned) {
        add(summoner.getName() + " summons a " + summoned.getName());
    }

    @Override
    public void raised(Character character) {
        add(character.getName() + " raises from its rests");
    }

    @Override
    public void intangibleTarget(Character character) {
        add("You cannot hit " + character.getName() + ", your weapons are useless");
    }

    @Override
    public void canNotShootSelf(Character character) {
        add("You can not shoot at yourself", Color.GRAY);
    }

    @Override
    public void inspectBookshelf(Item item) {
        add("You inspect the bookshelf and find " + item.getName());
    }

    @Override
    public void equipWeapon(Character character) {
        add(character.getName() + " wields a better weapon against you");
    }

    @Override
    public void playerMoves() {
        //log message unnecessary
    }

    @Override
    public void openInventory() {
        //log message unnecessary
    }

    @Override
    public void closeInventory() {
        //log message unnecessary
    }

    @Override
    public void poison() {
        //log message unnecessary
    }

    @Override
    public void castLightningStorm() {
        //log message unnecessary
    }

    @Override
    public void dispelledEffects(Character character) {
        add("All harmful effects on " + character.getName() + " are dispelled");
    }

    @Override
    public void foundCoins() {
        add("This is a enormous pile of coins, but you didn't see a single merchant during your journey", Color.YELLOW);
    }

    @Override
    public void attackByFire(Character attacker, Character victim, int damage) {
        add(attacker.getName() + " fire hits " + victim.getName() + " and deals " + damage + " of damage", Color.ORANGE);
    }

    @Override
    public void castFireWall() {
        //log message unnecessary
    }

    @Override
    public void endGame(PlayerHero player) {
        //log message unnecessary
    }

    @Override
    public void deathChangedPersona() {
        //log message unnecessary
    }

    public static class Line {
        public final String message;
        public final Color color;
        public int repeated;

        public Line(String message, Color color) {
            this.message = message;
            this.color = color;
            this.repeated = 1;
        }
    }
}
