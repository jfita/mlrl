/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.item.*;
import com.peritasoft.mlrl.ui.Button;
import com.peritasoft.mlrl.ui.FixedSizeButton;

public class InventoryRenderer implements Disposable {
    public static final int ITEMS_PER_PAGE = 20;

    private final SpriteBatch batch;
    private final BitmapFont textFont;

    private RendererAtlas atlas = new PrettyRendererAtlas();
    private TextureAtlas atlasInterface;
    private NinePatch inventoryBackground;
    private NinePatch listBackground;
    private NinePatch listBackgroundSelected;
    private NinePatch levelFull;
    private NinePatch levelEmpty;
    private Button button;
    private Button buttonDrop;
    private final PortraitMap portraits;
    private final FixedSizeButton buttonClose;

    public InventoryRenderer(SpriteBatch batch, BitmapFont textFont) {
        this.batch = batch;
        this.textFont = textFont;
        atlasInterface = new TextureAtlas("td_interface.atlas");
        inventoryBackground = new NinePatch(atlasInterface.findRegion("backgroundInventory"), 16, 16, 16, 16);
        listBackground = new NinePatch(atlasInterface.findRegion("backgroundList"), 10, 10, 5, 5);
        listBackgroundSelected = new NinePatch(atlasInterface.findRegion("backgroundListSelected"), 4, 4, 4, 4);
        levelFull = new NinePatch(atlasInterface.findRegion("levelFull"), 0, 0, 0, 0);
        levelEmpty = new NinePatch(atlasInterface.findRegion("levelEmpty"), 4, 4, 4, 4);
        button = new Button(104f, 56f, 86f,
                new NinePatch(atlasInterface.findRegion("buttonEmpty"), 8, 8, 4, 4),
                "");
        buttonDrop = new Button(104f, 38f, 86f,
                new NinePatch(atlasInterface.findRegion("buttonEmpty"), 8, 8, 4, 4),
                "Drop Item");
        buttonClose = new FixedSizeButton(368f, 217f, atlasInterface.findRegion("buttonCloseInventory"));
        portraits = new PortraitMap();
    }

    public void render(PlayerHero player, Item selected) {
        inventoryBackground.draw(batch, 8f, 32f + 2f, MyLittleRogueLike.SCREENWIDTH - 8f * 2f, MyLittleRogueLike.SCREENHEIGHT - 32f - 4f);
        renderInventory(selected, player);
        TextureAtlas.AtlasRegion portrait = portraits.get(player.getSex(), player.getDemography());
        renderCharacter(player, portrait, selected);
        buttonClose.draw(batch);
    }

    private void renderInventory(Item selected, PlayerHero player) {
        Inventory inventory = player.getInventory();
        Item equipped = player.getEquippedWeapon();
        int arrows = player.countArrows();

        float x;
        float y = 40f + (32f + 8f) * 4f;
        for (int row = 0, index = 0; row < 4; row++, y -= 32f + 8f) {
            x = 16f;
            for (int col = 0; col < 5; col++, x += 32f + 8f, index++) {
                Item item = index < inventory.size() ? inventory.get(index) : null;
                NinePatch listItem = listBackground;
                listItem.draw(batch, x, y, 32f, 32f);
                if (item != null && item == selected) {
                    listBackgroundSelected.draw(batch, x + 2, y + 3, 28f, 28f);
                }
                if (item != null) {
                    Sprite sprite = atlas.getItem(item.getCategory());
                    sprite.setPosition(x + 16f - sprite.getWidth() / 2f, y + 16f - sprite.getHeight() / 2f);
                    sprite.draw(batch);
                }
                if (item == equipped) {
                    textFont.setColor(0, 1, 0, 1);
                    textFont.draw(batch, "e", x + 32f - 11f, y + 16f);
                    textFont.setColor(1, 1, 1, 1);
                }
            }
        }

        x = 16f;
        y = 40f + 16f;
        listBackground.draw(batch, x, y, 72f, 16f);
        Sprite arrowSprite = atlas.getItem(ItemCategory.ARROWS);
        arrowSprite.setPosition(x + 2f, y + 1f);
        arrowSprite.draw(batch);
        textFont.setColor(Color.WHITE);
        textFont.draw(batch, " x " + arrows, x + 15f, y + 15f);

        x = 214f;
        y = 216f;
        float w = 170f;
        float h = 16f;
        listBackground.draw(batch, x, y, w - 18f, h);
        listBackground.draw(batch, x, y - h * 5f, w, h * 5f);
        if (selected != null) {
            String name = selected.getName();
            String description = selected.getDescription();
            if (selected instanceof Potion) {
                Potion potion = (Potion) selected;
                if (player.knowsPotion(potion.getColour())) {
                    name = potion.getKnownName();
                    description = potion.getKnownDescription();
                }
            }
            textFont.draw(batch, name, x + 3f, y + 15f);
            textFont.draw(batch, description.substring(0, Math.min(155, description.length())), x + 3f, y - h + 16f, w - 9f, Align.left, true);
        }
        renderButtons(selected, equipped);
    }

    private void renderButtons(Item selected, Item equipped) {
        if (selected == null) return;
        String label;
        if (selected.isEquipable()) {
            label = selected == equipped ? "Unequip" : "Equip";
        } else {
            label = "Use";
        }
        button.draw(batch, textFont, label);
        buttonDrop.draw(batch, textFont, "Drop Item");
    }

    public boolean buttonPressed(Vector2 position) {
        return button.pressed(position);
    }

    public boolean buttonDropPressed(Vector2 position) {
        return buttonDrop.pressed(position);
    }

    public boolean buttonClosePressed(Vector2 position) {
        return buttonClose.pressed(position);
    }

    private void renderCharacter(PlayerHero player, TextureAtlas.AtlasRegion portrait, Item selected) {
        float w = 170f;
        float h = 16f;
        float x = 214f;
        float y = 216f - h * 5f;
        listBackground.draw(batch, x, y - h * 6f, w, h * 6f);

        int bonusStr = 0;
        int bonusDex = 0;
        int bonusWis = 0;
        int bonusCon = 0;
        if (selected instanceof KindOfWeapon) {
            final KindOfWeapon kow = (KindOfWeapon) selected;
            final KindOfWeapon equipped = player.getEquippedWeapon();
            bonusStr = kow.getBonusStr() - equipped.getBonusStr();
            bonusDex = kow.getBonusDex() - equipped.getBonusDex();
            bonusWis = kow.getBonusWis() - equipped.getBonusWis();
            bonusCon = kow.getBonusCon() - equipped.getBonusCon();
        }


        batch.draw(portrait, x + 16f, y - 32f);
        textFont.draw(batch, player.getName(), x, y - 32f, 64f, Align.center, false);
        //textFont.draw(batch, player.getKlass().name().toLowerCase(), x, y - 43f, 48f, Align.center, false);

        float labelX = x + 58f;
        float labelY = y - 46f;
        float labelH = 12f;

        textFont.draw(batch, "HP:", labelX + 4f, y - 1f);
        textFont.setColor(Color.RED);
        // GWT does not support String.format() and must use string concatenation
        textFont.draw(batch, player.getHp() + "/" + player.getMaxHp(), labelX + 4f + 19f + 6f, y - 1f);
        textFont.setColor(Color.WHITE);
        textFont.draw(batch, "MP:", labelX + 4f, y - 1f - 11f);
        textFont.setColor(Color.BLUE);
        // ditto: GWT no String.format(); use concatenation
        textFont.draw(batch, player.getMp() + "/" + player.getMaxMp(), labelX + 4f + 19f + 6f, y - 1f - 11f);
        textFont.setColor(Color.WHITE);

        levelEmpty.draw(batch, labelX + 4f, y - 5f - 38f, 100f, 16f);
        levelFull.draw(batch, labelX + 4f, y - 5f - 38f, player.getXp(), 16f);
        textFont.draw(batch, "Level " + player.getLevel(), labelX + 4f, y - 5f - 25f + 1f, 100f, Align.center, false);

        drawStat(labelX - 32f, labelY - labelH * 0, selected, "Strength:", player.getStr(), bonusStr);
        drawStat(labelX - 32f, labelY - labelH * 1, selected, "Dextrery:", player.getDex(), bonusDex);
        drawStat(labelX - 32f, labelY - labelH * 2, selected, "Wisdom:", player.getWis(), bonusWis);
        drawStat(labelX - 32f, labelY - labelH * 3, selected, "Const.:", player.getCon(), bonusCon);
    }

    private void drawStat(float labelX, float labelY, Item item, String statName, int stat, int bonus) {
        float labelW = 57f;
        float spacing = 4f;
        textFont.draw(batch, statName, labelX, labelY, labelW, Align.right, false);
        textFont.draw(batch, String.valueOf(stat), labelX + labelW + spacing, labelY);
        if (item != null && item.isEquipable()) {
            textFont.setColor(bonus < 0 ? Color.RED : bonus == 0 ? Color.WHITE : Color.GREEN);
            // GWT does not support String.format and must do it manually
            textFont.draw(batch, "(" + (bonus < 0 || bonus == 0 ? "" : "+") + bonus + ")", labelX + labelW + spacing * 9, labelY);
            textFont.setColor(Color.WHITE);
        }
    }

    @Override
    public void dispose() {
        atlas.dispose();
        portraits.dispose();
        atlasInterface.dispose();
    }

    public int getCellPressed(Vector2 touchPoint) {
        float yMin = 32f + 32f + 8f;
        float xMin = 18f;
        float yMax = yMin + (4 * (32f + 8f));
        float xMax = xMin + (5 * (32f + 8f));

        if (touchPoint.x < xMin || touchPoint.x > xMax
                || touchPoint.y < yMin || touchPoint.y > yMax) {
            return -1;
        }

        int col = (int) ((touchPoint.x - xMin) / 40);
        int row = 3 - (int) ((touchPoint.y - yMin) / 40);

        return row * 5 + col; //careful! can be an empty cell
    }
}
