/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.ui.Button;
import com.peritasoft.mlrl.ui.Toggle;
import com.peritasoft.mlrl.ui.Widget;

public class MenuRenderer implements Disposable {
    private final BitmapFont textFont;
    private final TextureAtlas interfaceAtlas;
    private final NinePatch guiBackground;
    private final NinePatch selection;
    private final Toggle buttonMusic;
    private final Toggle buttonSfx;
    private final Toggle buttonTileset;
    private final Toggle buttonPad;
    private final Toggle buttonPadPos;
    private final Button buttonAbandon;
    private final Rectangle menu;
    private final Widget[] keyboardFocus;
    private int focusedObject;

    public MenuRenderer(BitmapFont textFont) {
        this.textFont = textFont;
        interfaceAtlas = new TextureAtlas(Gdx.files.internal("td_interface.atlas"));
        guiBackground = new NinePatch(interfaceAtlas.findRegion("backgroundGui", 1), 16, 16, 16, 16);
        selection = new NinePatch(interfaceAtlas.findRegion("selection"), 4, 4, 4, 4);
        final float w = 144f;
        final float h = 170f;
        menu = new Rectangle(MyLittleRogueLike.SCREENWIDTH - w, 32, w, h);

        NinePatch buttonNinePatch = new NinePatch(interfaceAtlas.findRegion("buttonEmpty"), 8, 8, 4, 4);
        final TextureRegion toggleOn = interfaceAtlas.findRegion("toggleOn");
        final TextureRegion toggleOff = interfaceAtlas.findRegion("toggleOff");
        final TextureRegion toggleLeft = interfaceAtlas.findRegion("toggleLeft");
        final TextureRegion toggleRight = interfaceAtlas.findRegion("toggleRight");
        buttonMusic = new Toggle(menu.x + menu.width - toggleOn.getRegionWidth() - 8f, menu.y + 138f, toggleOn, toggleOff);
        buttonSfx = new Toggle(menu.x + menu.width - toggleOn.getRegionWidth() - 8f, menu.y +114f, toggleOn, toggleOff);
        buttonTileset = new Toggle(menu.x + menu.width - toggleOn.getRegionWidth() - 8f, menu.y + 90f, toggleOn, toggleOff);
        buttonPad = new Toggle(menu.x + menu.width - toggleOn.getRegionWidth() - 8f, menu.y + 66f, toggleOn, toggleOff);
        buttonPadPos = new Toggle(menu.x + menu.width - toggleOn.getRegionWidth() - 8f, menu.y + 42f, toggleRight, toggleLeft);
        buttonAbandon = new Button(menu.x + 8f, menu.y + 12f, menu.width - 16f, buttonNinePatch, "Abandon Quest");
        keyboardFocus = new Widget[]{
                buttonMusic,
                buttonSfx,
                buttonTileset,
                buttonPad,
                buttonPadPos,
                buttonAbandon,
        };
        focusedObject = -1;
    }

    public void render(Batch batch, boolean musicOn, boolean sfxOn, boolean tilesetGraphic, boolean showPad, boolean rightPad) {
        guiBackground.draw(batch, menu.x, menu.y, menu.width, menu.height);
        textFont.setColor(batch.getColor());
        textFont.draw(batch, "Music", menu.x + 8f, buttonMusic.getY() + textFont.getLineHeight() + 5f, buttonMusic.getX() - menu.x - 14f, Align.right, false);
        buttonMusic.draw(batch, musicOn);
        textFont.draw(batch, "SFX", menu.x + 8f, buttonSfx.getY() + textFont.getLineHeight() + 5f, buttonSfx.getX() - menu.x - 14f, Align.right, false);
        buttonSfx.draw(batch, sfxOn);
        textFont.draw(batch, "Graphics", menu.x + 8f, buttonTileset.getY() + textFont.getLineHeight() + 5f, buttonTileset.getX() - menu.x - 14f, Align.right, false);
        buttonTileset.draw(batch, tilesetGraphic);
        textFont.draw(batch, "D-Pad", menu.x + 8f, buttonPad.getY() + textFont.getLineHeight() + 5f, buttonPad.getX() - menu.x - 14f, Align.right, false);
        buttonPad.draw(batch, showPad);
        textFont.draw(batch, "D-Pad Side", menu.x + 8f, buttonPadPos.getY() + textFont.getLineHeight() + 5f, buttonPadPos.getX() - menu.x - 14f, Align.right, false);
        buttonPadPos.draw(batch, rightPad);
        buttonAbandon.draw(batch, textFont);
        if (focusedObject >= 0) {
            Rectangle rect = getFocusedWidget().getRect();
            selection.draw(batch, rect.x, rect.y, rect.width, rect.height);
        }
    }

    private Widget getFocusedWidget() {
        return focusedObject >= 0 ? keyboardFocus[focusedObject] : null;
    }

    @Override
    public void dispose() {
        interfaceAtlas.dispose();
    }

    public Rectangle getRect() {
        return menu;
    }

    public float getHeight() {
        return menu.height;
    }

    public boolean buttonMusicPressed(Vector2 touchPoint) {
        return buttonMusic.pressed(touchPoint);
    }

    public boolean buttonMusicFocused() {
        return getFocusedWidget() == buttonMusic;
    }

    public boolean buttonSfxPressed(Vector2 touchPoint) {
        return buttonSfx.pressed(touchPoint);
    }

    public boolean buttonSfxFocused() {
        return getFocusedWidget() == buttonSfx;
    }

    public boolean buttonTilesetPressed(Vector2 touchPoint) {
        return buttonTileset.pressed(touchPoint);
    }

    public boolean buttonTilesetFocused() {
        return getFocusedWidget() == buttonTileset;
    }

    public boolean buttonPadPressed(Vector2 touchPoint) {
        return buttonPad.pressed(touchPoint);
    }

    public boolean buttonPadFocused() {
        return getFocusedWidget() == buttonPad;
    }

    public boolean buttonPadPosPressed(Vector2 touchPoint) {
        return buttonPadPos.pressed(touchPoint);
    }

    public boolean buttonPadPosFocused() {
        return getFocusedWidget() == buttonPadPos;
    }

    public boolean buttonAbandonPressed(Vector2 touchPoint) {
        return buttonAbandon.pressed(touchPoint);
    }

    public boolean buttonAbandonFocused() {
        return getFocusedWidget() == buttonAbandon;
    }

    public void focusNext() {
        if (focusedObject == -1 || focusedObject == keyboardFocus.length - 1) {
            focusedObject = 0;
        } else {
            focusedObject++;
        }
    }

    public void focusPrev() {
        if (focusedObject == -1 || focusedObject == 0) {
            focusedObject = keyboardFocus.length - 1;
        } else {
            focusedObject--;
        }
    }
}
