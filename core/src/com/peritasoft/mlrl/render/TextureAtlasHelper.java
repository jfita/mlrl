/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public final class TextureAtlasHelper {

    static Sprite mustCreateSprite(TextureAtlas atlas, String name) {
        return mustCreateSprite(atlas, name, -1);
    }

    static Sprite mustCreateSprite(TextureAtlas atlas, String name, int index) {
        final Sprite sprite = atlas.createSprite(name, index);
        if (sprite == null) {
            throw new IllegalArgumentException("Could not find '" + name + "' (index: " + index + ") region in " + atlas.toString());
        }
        return sprite;
    }

    public static TextureAtlas.AtlasRegion mustFindRegion(TextureAtlas atlas, String name) {
        return mustFindRegion(atlas, name, -1);
    }

    public static TextureAtlas.AtlasRegion mustFindRegion(TextureAtlas atlas, String name, int index) {
        final TextureAtlas.AtlasRegion region = atlas.findRegion(name, index);
       /*
        if (region == null) {
            throw new IllegalArgumentException("Could not find '" + name + "' (index: " + index + ") region in " + atlas.toString());
        }
        */
        return region;
    }

    public static TextureAtlas.AtlasRegion[] mustFindRegions(TextureAtlas atlas, String name, int firstIndex, int numFrames) {
        TextureAtlas.AtlasRegion[] regions = new TextureAtlas.AtlasRegion[numFrames];
        for (int frame = 0, index = firstIndex; frame < numFrames; frame++, index++) {
            regions[frame] = mustFindRegion(atlas, name, index);
        }
        return regions;
    }

    private TextureAtlasHelper() {
    }
}
