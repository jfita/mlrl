/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.LevelType;
import com.peritasoft.mlrl.effects.LifeObjType;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.props.PropType;

import java.util.HashMap;
import java.util.Map;

public abstract class RendererAtlas implements Disposable {
    private final Map<Demography, CharacterRenderer> characters = new HashMap<>();
    private final Map<LevelType, TerrainRenderer> terrains = new HashMap<>();
    private final Map<LifeObjType, ObjectRenderer> objects = new HashMap<>();
    private final Map<LifeObjType, ObjectRendererOffset> objectsOffset = new HashMap<>();
    private final Map<ItemCategory, Sprite> items = new HashMap<>();
    private final Map<PropType, PropRenderer> props = new HashMap<>();
    private final Map<ProjectileType, ProjectileRenderer> projectiles = new HashMap<>();
    private final Map<LifeObjType, AreaEffectRenderer> areaEffects = new HashMap<>();
    private final Map<ProjectileType, ImpactRenderer> impacts = new HashMap<>();
    private final Map<LifeObjType, StatusFxRenderer> statusesFx = new HashMap<>();
    private ImpactRenderer attackRenderer;

    public TerrainRenderer getTerrain(LevelType type) {
        return terrains.get(type);
    }

    public Sprite getItem(ItemCategory category) {
        return items.get(category);
    }

    public PropRenderer getProp(PropType type) {
        return props.get(type);
    }

    public ObjectRenderer getObject(LifeObjType type) {
        return objects.get(type);
    }

    public ObjectRendererOffset getObjectOffset(LifeObjType type) {
        return objectsOffset.get(type);
    }

    public CharacterRenderer getCharacter(Demography demography) {
        return characters.get(demography);
    }

    protected void loadCharacters(final TextureAtlas atlas) {
        characters.put(Demography.HERO_WARRIOR, new CharacterRenderer(atlas, "warrior"));
        characters.put(Demography.HERO_ARCHER, new CharacterRenderer(atlas, "archer"));
        characters.put(Demography.HERO_MAGE, new CharacterRenderer(atlas, "mage"));
        characters.put(Demography.GOBLIN_GRUNT, new CharacterRenderer(atlas, "goblin_gr"));
        characters.put(Demography.GOBLIN_WARRIOR, new CharacterRenderer(atlas, "goblin_sw"));
        characters.put(Demography.GOBLIN_LANCER, new CharacterRenderer(atlas, "goblin_spear"));
        characters.put(Demography.GOBLIN_MAGE, new CharacterRenderer(atlas, "goblin_mag"));
        characters.put(Demography.GOBLIN_ARCHER, new CharacterRenderer(atlas, "goblin_archer"));
        characters.put(Demography.TROLL, new CharacterRenderer(atlas, "troll"));
        characters.put(Demography.RAT_GIANT, new CharacterRenderer(atlas, "rat"));
        characters.put(Demography.BEETLE_GIANT, new CharacterRenderer(atlas, "beetle"));
        characters.put(Demography.SPIDER_GIANT, new CharacterRenderer(atlas, "spider"));
        characters.put(Demography.BAT_GIANT, new CharacterRenderer(atlas, "bat"));
        characters.put(Demography.SLIME, new CharacterRenderer(atlas, "slime"));
        characters.put(Demography.SLIME_CONJOINED, new CharacterRenderer(atlas, "slime_conjoined"));
        characters.put(Demography.SLIME_SMALL, new CharacterRenderer(atlas, "slime_small"));
        characters.put(Demography.SNAKE, new CharacterRenderer(atlas, "snake"));
        characters.put(Demography.JELLY_CUBE, new CharacterRenderer(atlas, "jelly"));
        characters.put(Demography.MIMIC, new CharacterRenderer(atlas, "mimic"));
        characters.put(Demography.TRASGA_HAMELIN, new CharacterRenderer(atlas, "witch"));
        characters.put(Demography.SKELETON, new CharacterRenderer(atlas, "skeleton_un"));
        characters.put(Demography.SKELETON_WARRIOR, new CharacterRenderer(atlas, "skeleton_sw"));
        characters.put(Demography.SKELETON_MAGE, new CharacterRenderer(atlas, "skeleton_mage"));
        characters.put(Demography.SKELETON_LICH, new CharacterRenderer(atlas, "skeleton_lich"));
        characters.put(Demography.SKELETON_ARCHER, new CharacterRenderer(atlas, "skeleton_archer"));
        characters.put(Demography.URMUK, new CharacterRenderer(atlas, "urmuk"));
        characters.put(Demography.BEHOLDER, new CharacterRenderer(atlas, "beholder"));
        characters.put(Demography.EKIMUS, new CharacterRenderer(atlas, "vampire"));
        characters.put(Demography.MINOTAUR, new CharacterRenderer(atlas, "minotaur"));
        characters.put(Demography.FIRE_ELEMENTAL, new CharacterRenderer(atlas, "flame"));
        characters.put(Demography.WRAITH, new CharacterRenderer(atlas, "wraith"));
        characters.put(Demography.GIANT, new CharacterRenderer(atlas, "giant"));
        characters.put(Demography.DWARF, new CharacterRenderer(atlas, "dwarf"));
        characters.put(Demography.IVREXA, new CharacterRenderer(atlas, "dragon"));
        characters.put(Demography.DEATH, new CharacterRenderer(atlas, "reaper"));
        characters.put(Demography.DARK_KNIGHT, new CharacterRenderer(atlas, "dark_knight"));
        characters.put(Demography.FIEND, new CharacterRenderer(atlas, "demon"));
        characters.put(Demography.PHOENIX, new CharacterRenderer(atlas, "phoenix"));
    }

    protected void loadTerrains(final TextureAtlas atlas) {
        terrains.put(LevelType.DUNGEON, new TerrainRenderer(atlas, "dungeon", "a"));
        terrains.put(LevelType.CRYPT, new TerrainRenderer(atlas, "crypt", "g"));
        terrains.put(LevelType.CRYPT2, new TerrainRenderer(atlas, "crypt", "h"));
        terrains.put(LevelType.SEWER, new TerrainRenderer(atlas, "sewer", "b"));
        terrains.put(LevelType.CAVE, new TerrainRenderer(atlas, "cave", "c"));
        terrains.put(LevelType.DEATH, new TerrainRenderer(atlas, "netherworld", "z"));
    }

    protected void loadObjects(final TextureAtlas atlas) {
        objects.put(LifeObjType.ENEMY_DEAD, new ObjectRenderer(atlas, "blood", 5));
        objects.put(LifeObjType.PLAYER_DEAD, new ObjectRenderer(atlas, "tombstone", 2));
        objects.put(LifeObjType.MAGIC_CIRCLE, new ObjectRenderer(atlas, "magic_circle", 5));
        objects.put(LifeObjType.BONES, new ObjectRenderer(atlas, "bones", 3));
        objectsOffset.put(LifeObjType.WEB, new ObjectRendererOffset(atlas, "web", 5));
    }

    protected void loadItems(final TextureAtlas atlas) {
        for (ItemCategory category : ItemCategory.values()) {
            items.put(category, TextureAtlasHelper.mustCreateSprite(atlas, category.name().toLowerCase()));
        }
        props.put(PropType.HEART_FULL, new SpritePropRenderer(atlas, "heart_full"));
        props.put(PropType.HEART_EMPTY, new SpritePropRenderer(atlas, "heart_empty"));
        props.put(PropType.COINS_COPPER, new SpritePropRenderer(atlas, "coins_copper"));
        props.put(PropType.COINS_SILVER, new SpritePropRenderer(atlas, "coins_silver"));
        props.put(PropType.COINS_GOLD, new SpritePropRenderer(atlas, "coins_gold"));
    }

    protected void loadProps(final TextureAtlas atlas) {
        props.put(PropType.CHEST_CLOSED, new SpritePropRenderer(atlas, "chest"));
        props.put(PropType.CHEST_OPEN, new SpritePropRenderer(atlas, "chest_open2"));
        props.put(PropType.BOOKSHELF_FULL, new SpritePropRenderer(atlas, "bookshelf_full"));
        props.put(PropType.BOOKSHELF_EMPTY, new SpritePropRenderer(atlas, "bookshelf_empty"));
        props.put(PropType.THRONE, new SpritePropRenderer(atlas, "throne", 1));
        props.put(PropType.STATUE_ANGEL, new SpritePropRenderer(atlas, "statue", 1));
        props.put(PropType.STATUE_WARRIOR, new SpritePropRenderer(atlas, "statue", 2));
        props.put(PropType.STATUE_DEMON, new SpritePropRenderer(atlas, "statue", 3));
        props.put(PropType.ALTAR, new SpritePropRenderer(atlas, "altar", 1));
        props.put(PropType.ALTAR_BONES, new SpritePropRenderer(atlas, "altar", 2));
        props.put(PropType.ALTAR_LEFT, new SpritePropRenderer(atlas, "altar", 3));
        props.put(PropType.ALTAR_CENTER, new SpritePropRenderer(atlas, "altar", 4));
        props.put(PropType.ALTAR_RIGHT, new SpritePropRenderer(atlas, "altar", 5));
        props.put(PropType.LIGHT_POINT, new SpritePropRenderer(atlas, "floor_transp"));
        props.put(PropType.CLAUDRON, new AnimatedPropsRenderer(atlas, "claudron"));
        props.put(PropType.TORCH, new TorchPropRenderer(atlas, "torch", new AnimatedPropsRenderer(atlas, "torchlight")));
        props.put(PropType.CANDLES, new AnimatedPropsRenderer(atlas, "candles"));
    }

    protected void loadFx(final TextureAtlas atlas) {
        for (ProjectileType type : ProjectileType.values()) {
            projectiles.put(type, new ProjectileRenderer(atlas, type.toString().toLowerCase()));
        }

        impacts.put(ProjectileType.ARROW, new ImpactRenderer(atlas, "pierce"));
        impacts.put(ProjectileType.FIREBALL, new ImpactRenderer(atlas, "fireimpact"));
        impacts.put(ProjectileType.POISONBALL, new ImpactRenderer(atlas, "poisonimpact"));
        impacts.put(ProjectileType.VOIDBALL, new ImpactRenderer(atlas, "voidimpact"));
        impacts.put(ProjectileType.ICEBALL, new ImpactRenderer(atlas, "iceimpact"));

        attackRenderer = new ImpactRenderer(atlas, "cut");

        areaEffects.put(LifeObjType.FIRE, new AreaEffectRenderer(atlas, "fireburn"));
        areaEffects.put(LifeObjType.ICE, new AreaEffectRenderer(atlas, "icespark"));
        areaEffects.put(LifeObjType.POISON, new AreaEffectRenderer(atlas, "poison"));
        areaEffects.put(LifeObjType.LIGHTNING, new AreaEffectRenderer(atlas, "lightning"));
        areaEffects.put(LifeObjType.VOID, new AreaEffectRenderer(atlas, "voidspark"));

        Map<Direction, Animation<Sprite>> animations = new HashMap<>();
        Animation<Sprite> animation = new Animation<>(0.5f,
                TextureAtlasHelper.mustCreateSprite(atlas, "smoke", 1),
                TextureAtlasHelper.mustCreateSprite(atlas, "smoke", 2)
        );
        for (Direction dir : Direction.values()) {
            animations.put(dir, animation);
        }
        characters.put(Demography.MIST, new CharacterRenderer(animations));

    }

    protected void loadStatusFx(TextureAtlas atlas) {
        statusesFx.put(LifeObjType.CONFUSED, new StatusFxRenderer(atlas, "confused", 13, 0.12f));
        statusesFx.put(LifeObjType.PARALYZED, new StatusFxRenderer(atlas, "paralyzed", 2, 0.12f));
        statusesFx.put(LifeObjType.FROZEN, new StatusFxRenderer(atlas, "frozen", 2, 0.30f));
        statusesFx.put(LifeObjType.MISS, new StatusFxRenderer(atlas, "miss", 6, 0.05f));
        statusesFx.put(LifeObjType.LVLUP, new StatusFxRenderer(atlas, "lvlup", 10, 0.12f));
    }

    public ProjectileRenderer getProjectile(ProjectileType type) {
        return projectiles.get(type);
    }

    public ImpactRenderer getImpactRenderer(ProjectileType type) {
        return impacts.get(type);
    }

    public ImpactRenderer getAttackRenderer() {
        return attackRenderer;
    }

    public AreaEffectRenderer getAreaEffectRenderer(LifeObjType type) {
        return areaEffects.get(type);
    }

    public StatusFxRenderer getStatusFxRenderer(LifeObjType type) {
        return statusesFx.get(type);
    }

    public abstract FloatingNumberRenderer getFloatingNumberRenderer();
}
