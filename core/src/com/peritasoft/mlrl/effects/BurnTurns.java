/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.events.GameEvent;

public class BurnTurns extends LifeTurnObj {
    private final int dmg;
    private final Level level;
    private final Character attacker;

    public BurnTurns(int dmg, Level level, Position position, Character attacker, int liveTurns) {
        super(position, liveTurns, LifeObjLayer.UNDER, LifeObjType.FIRE);
        this.dmg = dmg;
        this.level = level;
        this.attacker = attacker;
    }

    @Override
    public void update(boolean nextTurn) {
        super.update(nextTurn);
        if (nextTurn && isAlive()) {
            Cell cell = level.getCell(getPositionX(), getPositionY());
            final Character e = cell.getCharacter();
            if (e != null && e.isFlamable()) {
                GameEvent.attackByFire(attacker, e, dmg);
                e.receiveHit(e == attacker ? dmg/2 : dmg, attacker);
                e.setRegenHp(false);
            }
        }
    }
}
