/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Position;

import java.util.*;

public class PathFinder {
    public static Queue<Position> find(TraverseableMap level, Position startPos, Position goalPos) {
        return find(level, startPos, goalPos, level);
    }

    public static Queue<Position> find(Map level, Position startPos, Position goalPos, Traversor traversor) {
        if (!traversor.isTraverseable(goalPos.getX(), goalPos.getY())) {
            return null;
        }
        Node start = new Node(startPos);
        Node goal = new Node(goalPos);
        PriorityQueue<Node> frontier = new PriorityQueue<>();
        frontier.add(start);
        Node[][] cameFrom = new Node[level.getHeight()][level.getWidth()];
        int[][] cost = new int[level.getHeight()][level.getWidth()];
        for (int y = 0; y < level.getHeight(); y++) {
            Arrays.fill(cost[y], Integer.MAX_VALUE);
        }
        cost[start.y][start.x] = 0;

        while (!frontier.isEmpty()) {
            Node current = frontier.poll();

            if (current.equals(goal)) {
                break;
            }

            for (Direction dir : Direction.values()) {
                Node next = new Node(current, dir);
                if (!traversor.isTraverseable(next.x, next.y)) {
                    continue;
                }
                int newCost = cost[current.y][current.x] + current.cost(next);
                if (newCost < cost[next.y][next.x]) {
                    cost[next.y][next.x] = newCost;
                    next.setPriority(newCost + next.distanceTo(goal));
                    frontier.add(next);
                    cameFrom[next.y][next.x] = current;
                }
            }
        }
        if (cameFrom[goal.y][goal.x] == null) {
            return null;
        }
        Deque<Position> path = new ArrayDeque<>();
        Node current = goal;
        while (!current.equals(start)) {
            path.addFirst(current.toPosition());
            current = cameFrom[current.y][current.x];
        }
        return path;
    }

    public interface Map {
        int getHeight();

        int getWidth();
    }

    public interface Traversor {
        boolean isTraverseable(int x, int y);
    }

    public interface TraverseableMap extends Map, Traversor {
    }

    private static class Node implements Comparable<Node> {
        public final int x;
        public final int y;
        public int priority;

        Node(Position pos) {
            this(pos.getX(), pos.getY());
        }

        Node(Node current, Direction dir) {
            this(current.x + dir.x, current.y + dir.y);
        }

        Node(int x, int y) {
            this.x = x;
            this.y = y;
            priority = 0;
        }

        @Override
        public int compareTo(Node node) {
            return this.priority - node.priority;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;
            // Two nodes with the same position but different priorities are considered “equal”.
            // The priority is only used for ordering in PriorityQueue.
            if (x != node.x) return false;
            return y == node.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        public Integer cost(Node next) {
            return distanceTo(next);
        }

        public int distanceTo(Node goal) {
            return Math.abs(goal.x - this.x) + Math.abs(goal.y - this.y);
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public Position toPosition() {
            return new Position(x, y);
        }
    }
}
