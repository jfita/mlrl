/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.*;

public class EnemyPutterLowerCrypt extends EnemyPutter {

    public EnemyPutterLowerCrypt(int lowerBound, LevelGenerator next) {
        super(lowerBound, next);
    }

    @Override
    protected Character generateEnemy(int floor, int playerLevel, Position pos) {
        int enemyLevel = Math.max(floor, playerLevel);
        float rnd = MathUtils.random();
        if (rnd < 0.4) {
            return new Skeleton(enemyLevel, pos, true);
        } else if (rnd < 0.6) {
            return new SkeletonWarrior(enemyLevel + 1, pos);
        } else if (rnd < 0.8) {
            return new SkeletonArcher(enemyLevel, pos);
        } else if (rnd < 0.92) {
            return new SkeletonMage(enemyLevel, pos);
        } else {
            return new SkeletonLich(enemyLevel + 1, pos);
        }
    }
}
