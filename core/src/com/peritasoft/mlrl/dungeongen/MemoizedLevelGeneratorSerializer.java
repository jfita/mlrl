/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.serialization.MyLittleSerializer;

import java.util.ArrayList;

public class MemoizedLevelGeneratorSerializer implements Json.Serializer<MemoizedLevelGenerator> {

    @Override
    public void write(Json json, MemoizedLevelGenerator object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.getFloorWidth());
        json.writeValue(object.getFloorHeight());
        json.writeValue(object.isEndless());
        json.writeArrayStart();
        for (int i = 0; i < object.levels.size; i++) {
            MyLittleSerializer.writeObjectValue(json, object.levels.get(i));
        }
        json.writeArrayEnd();
        json.writeArrayEnd();
    }

    @Override
    public MemoizedLevelGenerator read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        final int floorWidth = iter.next().asInt();
        final int floorHeight = iter.next().asInt();
        final boolean isEndless = iter.next().asBoolean();
        final JsonValue.JsonIterator levelIter = iter.next().iterator();
        final ArrayList<Level> levels = new ArrayList<>();
        while (levelIter.hasNext()) {
            final Level level = MyLittleSerializer.readObjectValue(json, levelIter.next());
            levels.add(level);
        }
        LevelGenerator next;
        if (isEndless) {
            next = LevelGeneratorBuilder.buildEndlessLevelGenerator(floorWidth, floorHeight);
        } else {
            next = LevelGeneratorBuilder.buildLevelGenerator(floorWidth, floorHeight);
        }
        final MemoizedLevelGenerator levelGenerator = new MemoizedLevelGenerator(next, floorWidth, floorHeight, isEndless);
        levelGenerator.levels.addAll(levels.toArray(new Level[0]));
        return levelGenerator;
    }
}
