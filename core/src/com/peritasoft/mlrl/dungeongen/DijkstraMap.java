/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import java.util.Arrays;

public class DijkstraMap {
    private float[][] distances = new float[30][30];

    public void reset(int width, int height) {
        if (getHeight() != height || getWidth() != width) {
            distances = new float[height][width];
        }
        for (int row = 0; row < height; row++) {
            Arrays.fill(distances[row], Float.MAX_VALUE);
        }
    }

    public float getDistance(int row, int col) {
        return col >= 0 && col < getWidth() && row >= 0 && row < getHeight()
                ? distances[row][col]
                : Float.MAX_VALUE
                ;
    }

    public void setDistance(int row, int col, float distance) {
        if (col >= 0 && col < getWidth() && row >= 0 && row < getHeight()) {
            distances[row][col] = distance;
        }
    }

    public int getWidth() {
        return distances[0].length;
    }

    public int getHeight() {
        return distances.length;
    }
}
