/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.props.Prop;

public class Cell {
    public static final Cell BOUND = new Cell(Tile.BOUND, WallJoint.NONE);

    public Tile tile;
    public final WallJoint wallJoint;
    public int variation;
    private Prop prop;
    private Item item;
    private Character character;
    private float visibility;
    private boolean seen = false;
    private float fovRange;

    public Cell(Tile tile, WallJoint wallJoint) {
        this.tile = tile;
        this.wallJoint = wallJoint;
        if (tile == Tile.BOUND) {
            // The static BOUND variable screws up the random generated sequence because the first
            // time ever (i.e., from the program start) Level creates a Cell, Java first calls
            // the constructor for that variable; subsequent level generations using the same seed,
            // since that variable is already create, do a slightly different job and, hence, the
            // level is somewhat different.
            this.variation = 0;
        } else {
            this.variation = MathUtils.randomBoolean(0.90f) ? 0 : MathUtils.random(1, 5);
        }
    }

    public boolean hasCharacter() {
        return character != null;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public void removeCharacter() {
        setCharacter(null);
    }

    public boolean hasItem() {
        return item != null;
    }

    public Item pickupItem(Character character) {
        if (!character.shouldPickup(getItem())) {
            return null;
        }
        Item item = getItem();
        removeItem();
        return item;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    private void removeItem() {
        setItem(null);
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public boolean isWalkable() {
        return tile.isWalkable && (prop == null || prop.isWalkable());
    }

    public boolean blocksLight() {
        return tile.hindersSight || (prop != null && prop.blocksLight());
    }

    public boolean isTravesable() {
        return tile.isWalkable && seen;
    }

    public boolean hasProp() {
        return prop != null;
    }

    public Prop getProp() {
        return prop;
    }

    public void setProp(Prop prop) {
        this.prop = prop;
    }

    public void removeProp() {
        setProp(null);
    }

    public float getVisibility() {
        return Math.max(visibility, seen ? 0.25f : 0f);
    }

    public void setVisibility(float v) {
        this.seen = this.seen || v > 0f;
        this.visibility = v;
    }

    public void computeVisibility(int centerX, int centerY, int x, int y, float radius) {
        final float distance = (float) Position.distance(centerX, centerY, x, y);
        float fov = distance / radius;
        if (getFovRange() > fov) setFovRange(fov);
    }

    public void setFovRange(float range) {
        fovRange = range;
        setVisibility(1.0f - easing(range));
    }

    public float getFovRange() {
        return fovRange;
    }

    private float easing(float range) {
        return range * range * range * range;
    }

    public boolean isInFOV() {
        return this.fovRange < 1f;
    }

    public boolean isEmpty() {
        return !hasCharacter() && !hasItem() && !hasProp();
    }

    public boolean blocksSight() {
        return tile.hindersSight || !seen;
    }
}
