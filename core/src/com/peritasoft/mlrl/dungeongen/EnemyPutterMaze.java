/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.Minotaur;
import com.peritasoft.mlrl.characters.PlayerHero;

public class EnemyPutterMaze extends EnemyPutter {

    public EnemyPutterMaze(int lowerBound, LevelGenerator next) {
        super(lowerBound, next);
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        Level level = getNext().generate(floor, upStaircase, player);
        generateEnemies(getLowerBound(), level, floor, player.getLevel());
        return level;
    }

    @Override
    protected Position findValidPosition(Level level) {
        final int height = level.getHeight();
        final int width = level.getWidth();
        while (true) {
            Position pos = new Position(MathUtils.random(width - 1), MathUtils.random(height - 1));
            if (level.isGround(pos)) {
                return pos;
            }
        }
    }

    @Override
    protected Character generateEnemy(int floor, int playerLevel, Position pos) {
        int enemyLevel = Math.max(floor, playerLevel);
        return new Minotaur(enemyLevel, pos);
    }
}
