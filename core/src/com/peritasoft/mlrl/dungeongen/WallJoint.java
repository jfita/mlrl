/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

public enum WallJoint {
    NONE(false, false, false, false),
    R(false, false, false, true),
    B(false, false, true, false),
    BR(false, false, true, true),
    L(false, true, false, false),
    LR(false, true, false, true),
    LB(false, true, true, false),
    LBR(false, true, true, true),
    T(true, false, false, false),
    TR(true, false, false, true),
    TB(true, false, true, false),
    TBR(true, false, true, true),
    TL(true, true, false, false),
    TLR(true, true, false, true),
    TLB(true, true, true, false),
    TLBR(true, true, true, true),
    ;

    public final boolean top;
    public final boolean left;
    public final boolean bottom;
    public final boolean right;

    WallJoint(boolean top, boolean left, boolean bottom, boolean right) {
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }


    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    public int toInt() {
        int i = 0;
        for (WallJoint value : values()) {
            if (this == value) {
                break;
            }
            i++;
        }
        return i;
    }

    public static WallJoint find(boolean top, boolean left, boolean bottom, boolean right) {
        for (WallJoint join : WallJoint.values()) {
            if (join.top == top && join.left == left && join.bottom == bottom && join.right == right) {
                return join;
            }
        }
        return NONE;
    }
}
