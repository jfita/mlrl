/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.PlayerHero;

public class RoomsLevelGenerator implements LevelGenerator {
    private Tile[][] tiles;
    private final int width;
    private final int height;
    private final LevelType levelType;

    public RoomsLevelGenerator(int width, int height, LevelType levelType) {
        this.width = width;
        this.height = height;
        this.levelType = levelType;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        setUp(width, height);
        for (int tries = 1; tries <= 1000; tries++) {
            Position roomIni = new Position(width / 2, height / 2);
            Position directionVector = new Position(1, 1);
            if (createRoom(new Room(roomIni, directionVector))) {
                break;
            }
        }
        return tearDown(upStaircase);
    }

    @SuppressWarnings("UnusedAssignment")
    private boolean createRoom(final Room room) {
        if (!fits(room)) {
            return false;
        }
        carve(room);

        float chance = 0.40f;
        chance += MathUtils.randomBoolean(chance) && tryCarveNorth(room) ? -0.20f : 0.20f;
        chance += MathUtils.randomBoolean(chance) && tryCarveSouth(room) ? -0.20f : 0.20f;
        chance += MathUtils.randomBoolean(chance) && tryCarveEast(room) ? -0.20f : 0.20f;
        chance += MathUtils.randomBoolean(chance) && tryCarveWest(room) ? -0.20f : 0.20f;

        return true;
    }

    private boolean tryCarveNorth(final Room room) {
        return tryCarveVertical(room.north, 1, room);
    }

    private boolean tryCarveSouth(final Room room) {
        return tryCarveVertical(room.south, -1, room);
    }

    private boolean tryCarveEast(final Room room) {
        return tryCarveHorizontal(room.east, 1, room);
    }

    private boolean tryCarveWest(final Room room) {
        return tryCarveHorizontal(room.west, -1, room);
    }

    private boolean tryCarveVertical(final int y, final int up, final Room prev) {
        for (int tries = 1; tries <= 1000; tries++) {
            final Position pos = new Position(MathUtils.random(prev.west + 3, prev.east - 3), y);
            final Position dir = new Position(MathUtils.randomSign(), up);
            final Room room = new Room(pos, dir);
            if (createRoom(room)) {
                final int west = Math.max(prev.west, room.west) + 1;
                final int east = Math.min(prev.east, room.east) - 1;
                tiles[y][MathUtils.random(west, east)] = Tile.GROUND;
                return true;
            }
        }
        return false;
    }

    private boolean tryCarveHorizontal(final int x, final int side, final Room prev) {
        for (int tries = 1; tries <= 1000; tries++) {
            final Position pos = new Position(x, MathUtils.random(prev.south + 3, prev.north - 3));
            final Position dir = new Position(side, MathUtils.randomSign());
            final Room room = new Room(pos, dir);
            if (createRoom(room)) {
                final int south = Math.max(prev.south, room.south) + 1;
                final int north = Math.min(prev.north, room.north) - 1;
                tiles[MathUtils.random(south, north)][x] = Tile.GROUND;
                return true;
            }
        }
        return false;
    }

    private void carve(final Room room) {
        for (int y = room.south + 1; y < room.north; y++) {
            tiles[y][room.east] = Tile.WALL;
            tiles[y][room.west] = Tile.WALL;
            for (int x = room.west + 1; x < room.east; x++) {
                tiles[y][x] = Tile.GROUND;
            }
        }
        for (int x = room.west; x <= room.east; x++) {
            tiles[room.south][x] = Tile.WALL;
            tiles[room.north][x] = Tile.WALL;
        }
    }

    private boolean fits(final Room room) {
        for (int y = room.south; y <= room.north; y++) {
            for (int x = room.west; x <= room.east; x++) {
                if (isOccupied(x, y)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isOccupied(final int x, final int y) {
        if (x < 0 || x >= tiles[0].length || y < 0 || y >= tiles.length) {
            return true;
        }
        return tiles[y][x] == Tile.GROUND;
    }

    private void setUp(final int width, final int height) {
        tiles = new Tile[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                tiles[y][x] = Tile.BLANK;
            }
        }
    }

    private Level tearDown(final boolean upStaircase) {
        Position startingPosition = LevelUtils.putStaircases(tiles, upStaircase, true);
        Level level = new Level(tiles, startingPosition, levelType);
        tiles = null;
        return level;
    }

    private static class Room {
        final int south;
        final int west;
        final int north;
        final int east;

        private static final int MIN_WIDTH = 8;
        private static final int MAX_WIDTH = 12;
        private static final int MIN_HEIGHT = 8;
        private static final int MAX_HEIGHT = 12;

        Room(final Position pos, final Position dir) {
            final int h = MathUtils.random(MIN_HEIGHT, MAX_HEIGHT);
            south = Math.min(pos.getY(), pos.getY() + (h - 1) * dir.getY());
            north = Math.max(pos.getY(), pos.getY() + (h - 1) * dir.getY());
            final int w = MathUtils.random(MIN_WIDTH, MAX_WIDTH);
            west = Math.min(pos.getX(), pos.getX() + (w - 1) * dir.getX());
            east = Math.max(pos.getX(), pos.getX() + (w - 1) * dir.getX());
        }
    }

}
