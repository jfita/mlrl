/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

public class Geometry {
    public static boolean walkLine(Position from, Position to, LineWalker walker) {
        from = new Position(from);
        int dx = Math.abs(to.getX() - from.getX());
        int dy = Math.abs(to.getY() - from.getY());

        int sx = from.getX() < to.getX() ? 1 : -1;
        int sy = from.getY() < to.getY() ? 1 : -1;

        int err = dx - dy;
        while (!from.equals(to)) {
            int e2 = 2 * err;
            if (e2 > -1 * dy) {
                err = err - dy;
                from.setX(from.getX() + sx);
            }
            if (e2 < dx) {
                err = err + dx;
                from.setY(from.getY() + sy);
            }
            if (!walker.walk(from)) return false;
        }
        return true;
    }

    public interface LineWalker {
        boolean walk(Position p);
    }
}
