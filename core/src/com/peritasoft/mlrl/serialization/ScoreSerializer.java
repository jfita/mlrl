/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.Sex;
import com.peritasoft.mlrl.scores.Score;

public class ScoreSerializer implements Json.Serializer<Score> {

    @Override
    public void write(Json json, Score object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.sex);
        json.writeValue(object.player);
        json.writeValue(object.killer);
        json.writeValue(object.floor);
        json.writeValue(object.dateTime);
        json.writeValue(object.runTime);
        json.writeArrayEnd();
    }

    @Override
    public Score read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        Sex sex = json.readValue(Sex.class, iter.next());
        Demography player = json.readValue(Demography.class, iter.next());
        Demography killer = json.readValue(Demography.class, iter.next());
        int floor = iter.next().asInt();
        String dateTime = iter.next().asString();
        long runTime = iter.next().asLong();
        return new Score(sex, player, killer, floor, dateTime, runTime);
    }
}
