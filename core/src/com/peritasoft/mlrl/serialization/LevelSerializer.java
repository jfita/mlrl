/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.*;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.props.Prop;

import java.util.ArrayList;

public abstract class LevelSerializer<T extends Level> implements Json.Serializer<T> {
    @Override
    public void write(Json json, T object, Class knownType) {
        final int width = object.getWidth();
        final int height = object.getHeight();

        json.writeArrayStart();
        json.writeValue(object.getType());
        json.writeValue(width);
        json.writeValue(height);
        json.writeValue(object.getStartingPosition());

        final ArrayList<PlacedObject<Prop>> props = new ArrayList<>();
        final ArrayList<PlacedObject<Item>> items = new ArrayList<>();
        final ArrayList<Character> characters = new ArrayList<>();
        json.writeArrayStart();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final Cell cell = object.getCell(x, y);
                final int seen = cell.getVisibility() > 0f ? 1 : 0;
                final int variation = cell.variation;
                json.writeValue(variation << 9 | seen << 8 | cell.tile.ordinal());
                if (cell.hasProp()) {
                    props.add(new PlacedObject<>(x, y, cell.getProp()));
                }
                if (cell.hasItem()) {
                    items.add(new PlacedObject<>(x, y, cell.getItem()));
                }
                if (cell.hasCharacter()) {
                    final Character character = cell.getCharacter();
                    if (!(character instanceof PlayerHero)) {
                        characters.add(character);
                    }
                }
            }
        }
        json.writeArrayEnd();
        json.writeArrayStart();
        for (final PlacedObject<Prop> prop : props) {
            prop.write(json);
        }
        json.writeArrayEnd();
        json.writeArrayStart();
        for (final PlacedObject<Item> item : items) {
            item.write(json);
        }
        json.writeArrayEnd();
        json.writeArrayStart();
        for (final Character character : characters) {
            MyLittleSerializer.writeObjectValue(json, character);
        }
        json.writeArrayEnd();

        writeOwnProperties(json, object);

        json.writeArrayEnd();
    }

    protected abstract void writeOwnProperties(Json json, T object);

    @Override
    public T read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        final LevelType levelType = json.readValue(LevelType.class, iter.next());
        final int width = iter.next().asInt();
        final int height = iter.next().asInt();
        final Position startingPosition = json.readValue(Position.class, iter.next());

        final JsonValue.JsonIterator tileIter = iter.next().iterator();
        final Tile[] availableTiles = Tile.values();
        Tile[][] tiles = new Tile[height][width];
        boolean[][] seen = new boolean[height][width];
        int[][] variations = new int[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final int tileInfo = tileIter.next().asInt();
                tiles[y][x] = availableTiles[tileInfo & 0xff];
                seen[y][x] = (tileInfo & 0x100) == 0x100;
                variations[y][x] = (tileInfo >> 9);
            }
        }

        final ArrayList<PlacedObject<Prop>> props = new ArrayList<>();
        for (final JsonValue propValue : iter.next().iterator()) {
            final PlacedObject<Prop> prop = PlacedObject.read(json, propValue);
            props.add(prop);
        }
        final ArrayList<PlacedObject<Item>> items = new ArrayList<>();
        for (final JsonValue itemValue : iter.next().iterator()) {
            final PlacedObject<Item> item = PlacedObject.read(json, itemValue);
            items.add(item);
        }
        final ArrayList<Character> characters = new ArrayList<>();
        for (final JsonValue characterValue : iter.next().iterator()) {
            final Character character = MyLittleSerializer.readObjectValue(json, characterValue);
            characters.add(character);
        }

        final T level = createInstance(iter, tiles, startingPosition, levelType);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final Cell cell = level.getCell(x, y);
                cell.setVisibility(seen[y][x] ? 1f : 0f);
                cell.variation = variations[y][x];
            }
        }
        for (final Character character : characters) {
            level.addEnemy(character);
        }
        for (final PlacedObject<Prop> prop : props) {
            level.getCell(prop.x, prop.y).setProp(prop.object);
        }
        for (final PlacedObject<Item> item : items) {
            level.getCell(item.x, item.y).setItem(item.object);
        }
        return level;
    }

    protected abstract T createInstance(JsonValue.JsonIterator iter, Tile[][] tiles, Position startingPosition, LevelType levelType);

    private static class PlacedObject<T> {
        public final int x;
        public final int y;
        public final T object;

        private PlacedObject(int x, int y, final T object) {
            this.x = x;
            this.y = y;
            this.object = object;
        }

        public void write(Json json) {
            json.writeArrayStart();
            json.writeValue(x);
            json.writeValue(y);
            MyLittleSerializer.writeObjectValue(json, object);
            json.writeArrayEnd();
        }

        public static <T> PlacedObject<T> read(Json json, JsonValue jsonData) {
            final JsonValue.JsonIterator iter = jsonData.iterator();
            final int x = iter.next().asInt();
            final int y = iter.next().asInt();
            final T object = MyLittleSerializer.readObjectValue(json, iter.next());
            return new PlacedObject<>(x, y, object);
        }
    }
}
