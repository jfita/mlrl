/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Dungeon;
import com.peritasoft.mlrl.events.Log;
import com.peritasoft.mlrl.screens.PlayScreen;

public class PlayScreenSerializer implements Json.Serializer<PlayScreen> {
    private final MyLittleRogueLike game;

    public PlayScreenSerializer(MyLittleRogueLike game) {
        this.game = game;
    }

    @Override
    public void write(Json json, PlayScreen object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.getSeed());
        json.writeValue(object.getPlayer());
        json.writeValue(object.getDungeon());
        json.writeValue(object.getLog());
        json.writeArrayEnd();
    }

    @Override
    public PlayScreen read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        final long seed = iter.next().asLong();
        final PlayerHero playerHero = json.readValue(PlayerHero.class, iter.next());
        final Dungeon dungeon = json.readValue(Dungeon.class, iter.next());
        final Log log = json.readValue(Log.class, iter.next());
        final PlayScreen object = new PlayScreen(game, playerHero, dungeon, log, seed, false);
        object.deserialize();
        return object;
    }
}
