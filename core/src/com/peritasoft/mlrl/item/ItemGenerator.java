/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.badlogic.gdx.math.MathUtils;

public class ItemGenerator {

    public static Item generateScroll(int level) {
        return generateScroll(MathUtils.random(4), level);
    }

    public static Item generateScroll(int option, int level) {
        switch (option) {
            case 0:
                return new ScrollOfFire(level);
            case 1:
                return new ScrollOfIce(level);
            case 2:
                return new ScrollOfPetrification(level);
            case 3:
                return new ScrollOfPoison(level);
            case 4:
                return new ScrollOfTeleport(level);
            default:
                throw new IllegalArgumentException("No such scroll: " + option);
        }
    }
}
