/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;

public class Arrow extends Item {
    private final int count;

    public Arrow() {
        this(1);
    }

    public Arrow(int count) {
        super(count == 1 ? "Arrow" : count + " Arrows", "Just a sharp bunch of arrows. Be careful to take it from the correct side.");
        this.count = count;
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.ARROWS;
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public boolean isUsable() {
        return false;
    }

    @Override
    public void use(Character character, Level level) {
        throw new IllegalStateException("Arrows can not be used directly");
    }

    public int countArrows() {
        return count;
    }
}
